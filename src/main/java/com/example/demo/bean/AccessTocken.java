package com.example.demo.bean;

import lombok.Data;

@Data
public class AccessTocken {

    private String access_token;
    private String expires_in;
}
