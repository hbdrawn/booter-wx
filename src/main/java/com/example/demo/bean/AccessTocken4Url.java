package com.example.demo.bean;

import lombok.Data;

@Data
public class AccessTocken4Url {

    private String access_token;
    private String expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
    private String unionid;
    private String session_key;
}
