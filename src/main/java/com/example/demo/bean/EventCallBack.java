package com.example.demo.bean;

import com.example.demo.utils.xml.CDATAConvert;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import lombok.Data;

@Data
@XStreamAlias("xml")
public class EventCallBack {

    @XStreamConverter(CDATAConvert.class)
    private String ToUserName;
    @XStreamConverter(CDATAConvert.class)
    private String FromUserName;
    @XStreamConverter(CDATAConvert.class)
    private String CreateTime;
    @XStreamConverter(CDATAConvert.class)
    private String MsgType;
    @XStreamConverter(CDATAConvert.class)
    private String Event;
    @XStreamConverter(CDATAConvert.class)
    private String EventKey;
    @XStreamConverter(CDATAConvert.class)
    private String Ticket;
}
