package com.example.demo.bean;

import java.util.List;

import com.example.demo.entity.Orders;

import lombok.Data;

@Data
public class OrdersDisplay {

    private Float totalFee = 0F;// 总金额

    private Integer orderNums = 0;// 订单总数

    List<Orders> ordersList;// 订单列表
}
