package com.example.demo.bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.example.demo.entity.Orders;

import lombok.Data;

@Data
public class OrdersDisplay4Mlogin {

    private String account;

    private Integer month = Calendar.getInstance().get(Calendar.MONTH); // 0-11

    private Integer year = Calendar.getInstance().get(Calendar.YEAR); // 0-11

    private Float totalFee = 0F;// 总销售额

    private Integer orderNums = 0;// 订单总数

    private Integer subAccount = 0;// 有效子用户个数

    private List<Orders> orders = new ArrayList<>();// 用户综合信息列表，只有用户用户需要

    private List<SubDoctorDisplay> subDoctorList = new ArrayList<>();// 用户综合信息列表

    private List<SubOrdersDisplay> subOrdersList = new ArrayList<>();// 以产品为主的销售信息
}
