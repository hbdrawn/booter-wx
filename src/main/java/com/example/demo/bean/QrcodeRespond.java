package com.example.demo.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 永久二维码应答体
 */
@Setter
@Getter
@NoArgsConstructor
public class QrcodeRespond extends WxErrorCode {

    private String ticket;
    private String expire_seconds;
    private String url;
}
