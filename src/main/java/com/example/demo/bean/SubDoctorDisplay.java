package com.example.demo.bean;

import lombok.Data;

@Data
public class SubDoctorDisplay {
    private String headImgUrl;
    private String nickName;
    private String account;
    private String phone;
    private Float totalFee = 0F;
}