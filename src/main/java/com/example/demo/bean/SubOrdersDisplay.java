package com.example.demo.bean;

import lombok.Data;

@Data
public class SubOrdersDisplay {
    private String picThumbPath;
    private String title;
    private Long itemId;
    private Float totalFee = 0F;
}