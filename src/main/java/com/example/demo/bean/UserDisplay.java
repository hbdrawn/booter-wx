package com.example.demo.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserDisplay implements Serializable {

    private static final long serialVersionUID = -617585786255973836L;
    private String username;
    private String password;
    private Integer userType;
    private String wxOpenId;
}
