package com.example.demo.bean;

import lombok.Data;

@Data
public class WxErrorCode {

    private Integer errcode;
    private String errmsg;
}
