
package com.example.demo.bean;

import java.util.List;

import com.example.demo.entity.CustomerList;

import lombok.Data;

/**
 * 描述:
 * 
 * @author zhangyj
 * @date 2018年6月23日 下午3:57:50
 * @version CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Data
public class WxUserInfo {

    /**
     * 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
     * 
     **/
    private Integer subscribe;
    private String openid;
    private String nickname;
    /*
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;
    private String language;
    private String city;
    private String province;
    private String country;
    private String headimgurl;
    private Long subscribe_time;
    private String unionid;
    private String remark;
    private String groupid;
    private List<Integer> tagid_list;
    /**
     * 返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION
     * 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE
     * 扫描二维码，ADD_SCENEPROFILE LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM
     * 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他
     */
    private String subscribe_scene;
    private Long qr_scene;
    private String qr_scene_str;

    public CustomerList transfer() {
        CustomerList newInfo = new CustomerList();
        newInfo.setCity(city);
        newInfo.setCountry(country);
        newInfo.setGroupid(groupid);
        newInfo.setHeadimgurl(headimgurl);
        newInfo.setLanguage(language);
        newInfo.setNickname(nickname);
        // newInfo.setOpenid(openid);
        newInfo.setProvince(province);
        newInfo.setQr_scene(qr_scene);
        newInfo.setQr_scene_str(qr_scene_str);
        newInfo.setRemark(remark);
        newInfo.setSex(sex);
        newInfo.setSubscribe(subscribe);
        if (tagid_list != null && tagid_list.size() > 0) {
            newInfo.setTagid_list(tagid_list.toString());
        }
        newInfo.setUnionid(unionid);
        newInfo.setSubscribe_scene(subscribe_scene);
        newInfo.setSubscribe_time(subscribe_time);
        return newInfo;
    }

}
