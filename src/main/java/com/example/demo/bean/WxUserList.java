
package com.example.demo.bean;

import java.util.List;

import lombok.Data;

/**
 * 描述:
 * 
 * @author zhangyj
 * @date 2018年6月23日 下午3:57:50
 * @version CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Data
public class WxUserList {

    private Long total;
    private Long count;
    private UserData data;
    private String next_openid;

    public List<String> getIds() {
        return data.getOpenid();
    }
}

@Data
class UserData {
    private List<String> openid;
}
