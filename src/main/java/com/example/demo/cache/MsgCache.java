
package com.example.demo.cache;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.example.demo.utils.JsonUtils;
import com.example.realtime.MsgBody;
import com.example.realtime.MsgWrapper;
import com.example.realtime.utils.ConversationsType;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年9月8日 下午5:35:36
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/

public class MsgCache {

    public static Map<String, Map<String, List<MsgWrapper>>> msgCache = new ConcurrentHashMap<>();
    static {
        for (int i = 0; i < 10; i++) {
            MsgWrapper msgs = new MsgWrapper();
            msgs.setType(ConversationsType.text);
            msgs.setContent(i + "周末有时间一起参加社团活动吗?");
            msgs.setFriendHeadUrl(
                    "http://thirdwx.qlogo.cn/mmopen/hDiaI7p3XEaELTQsQgwk3fQ7q7tSaV3VFONW9BCCp10bRI8yAEiavhy2hibRBicd0MUFNGIX4iag5Yaic8KH7x6QLMKicoOgCOiakPIm/132");
            msgs.setFriendId("oKhLb0cjwVI8Oa5c-_xlwcslr36g");
            msgs.setFriendName("高远");
            MsgBody msgBody = new MsgBody();
            msgBody.setType(msgs.getType().name());
            msgBody.setContent(msgs.getContent());
            String serial = JsonUtils.serial(msgBody);
            msgs.setLatestMsg(serial.replaceAll("/", ""));
            msgs.setMsgUserHeadUrl(
                    "http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaELTvVqrcYTD3CibdbzG4BwMrXILqqmyXeSRey7ccVY7ick6ibllv4YiaR2QhTdJiaVRfichgmGtR2WKVXkA/132");
            msgs.setMsgUserId("oKhLb0R_1oeSWc9AHNX2GFoOFR_Q");
            msgs.setMsgUserName("hbdrawn");
            msgs.setTimestamp(new Date());
            put("oKhLb0R_1oeSWc9AHNX2GFoOFR_Q", "oKhLb0cjwVI8Oa5c-_xlwcslr36g", msgs, true);
        }
    }

    public static void put(String openId, String friendOpenId, MsgWrapper msg, boolean incUnread) {
        Map<String, List<MsgWrapper>> map = msgCache.get(openId);
        if (map == null) {
            map = new ConcurrentHashMap<>();
            msgCache.put(openId, map);
        }

        List<MsgWrapper> list = map.get(friendOpenId);
        if (list == null) {
            list = new CopyOnWriteArrayList<>();
            map.put(friendOpenId, list);
        }
        if (incUnread) {
            if (!list.isEmpty()) {
                msg.setUnread(list.get(list.size() - 1).getUnread() + 1);
            } else {
                msg.setUnread(1);
            }
        }

        list.add(msg);
    }
}
