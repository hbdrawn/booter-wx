package com.example.demo.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.AccountService;
import com.example.framework.config.SpringUtil;
import com.example.realtime.utils.UserInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年8月19日 下午9:57:40
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Slf4j
public class RTAccountCache {

    public static Map<String, Account> accountsMap = new ConcurrentHashMap<>();

    public static Map<String, CustomerList> customerMap = new ConcurrentHashMap<>();

    // public static Map<String, List<CustomerList>> doctorsSubscribMap = new
    // ConcurrentHashMap<>();
    /**
     * .存放关系对
     */
    public static Map<String, List<String>> subscribeMap = new ConcurrentHashMap<>();

    public static Map<String, String> unionIdWithOpenIdMap = new ConcurrentHashMap<>();

    public static void addDoctors(String customerOpenId, Account doctor) {
        accountsMap.put(customerOpenId, doctor);
    }

    public static void addDoctorSubcribe(String doctorOpenId, String subcribeOpenId) {
        List<String> list = subscribeMap.get(doctorOpenId);
        if (list == null) {
            list = new CopyOnWriteArrayList<>();
            subscribeMap.put(doctorOpenId, list);
        }
        list.add(subcribeOpenId);
    }

    public static void load() {
        AccountService accountService = SpringUtil.getBean(AccountService.class);
        List<Account> findAllByType = accountService.findAllByType(2);
        Map<String, Account> allDoctors = new HashMap<>();
        for (Account tmp : findAllByType) {
            if (tmp.getOpenId() != null && tmp.getState() == 1) {
                allDoctors.put(tmp.getAccount(), tmp);
            }
        }

        CustomerRepository customerRepository = SpringUtil.getBean(CustomerRepository.class);
        List<CustomerList> findAllByStatus = customerRepository.findAllByStatus(1);
        for (CustomerList tmp : findAllByStatus) {
            Account account = allDoctors.get(tmp.getAccount());
            if (tmp.getQr_scene_str() != null && account != null) {
                addDoctors(account.getOpenId(), account);
                addDoctorSubcribe(account.getOpenId(), tmp.getFromUserName());
                addDoctorSubcribe(tmp.getFromUserName(), account.getOpenId());
                customerMap.put(tmp.getFromUserName(), tmp);
                if (tmp.getUnionid() != null) {
                    unionIdWithOpenIdMap.put(tmp.getUnionid(), tmp.getFromUserName());
                } else {
                    log.warn("用户[{}][{}]无unionId", tmp.getNickname(), tmp.getAccount());
                }
                if (account.getUnionId() != null) {
                    unionIdWithOpenIdMap.put(account.getUnionId(), account.getOpenId());
                } else {
                    log.warn("用户[{}]无unionId", account.getAccount());
                }
            }
        }
        log.info("加载用户缓存成功");

    }

    /**
     * .描述:websocket用户登陆，获取登陆信息，并返回
     *
     * @param openId
     * @return
     * 
     * @author zhangyj
     * @date 2018年9月8日 下午6:05:23
     * @version 1.0.0
     * 
     *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
     *
     **/
    public static UserInfo getUserInfo(String openId) {
        UserInfo userInfo = null;
        Account account = accountsMap.get(openId);
        String userType = "医生用户";
        if (account == null) {
            CustomerList customer = customerMap.get(openId);
            if (customer != null) {
                userType = "关注用户";
                userInfo = new UserInfo();
                userInfo.setUserId(openId);
                userInfo.setNickName(customer.getNickname());
                userInfo.setMyHeadUrl(customer.getHeadimgurl());
                userInfo.setUserType(2);
            }
        } else {
            userInfo = new UserInfo();
            userInfo.setUserId(openId);
            userInfo.setNickName(account.getNickname());
            userInfo.setMyHeadUrl(account.getHeadimgurl());
            userInfo.setUserType(1);
        }
        if (userInfo != null) {
            log.info("{}[{}|{}]拉取用户信息成功", userType, userInfo.getUserId(), userInfo.getNickName());
        }
        return userInfo;
    }

    /**
     * .描述:获取朋友列表
     *
     * @param openId
     * @return
     * 
     * @author zhangyj
     * @date 2018年9月8日 下午6:10:09
     * @version 1.0.0
     * 
     *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
     *
     **/
    public static List<String> getFriendList(String openId) {
        return subscribeMap.get(openId);
    }

    /**
     * .描述:获取朋友列表
     *
     * @param openId
     * @return
     * 
     * @author zhangyj
     * @date 2018年9月8日 下午6:10:09
     * @version 1.0.0
     * 
     *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
     *
     **/
    public static List<UserInfo> getFriends(String openId) {
        List<UserInfo> friends = new ArrayList<>();
        List<String> list = subscribeMap.get(openId);
        if (list != null) {
            for (String friendOpenId : list) {
                UserInfo userInfo = getUserInfo(friendOpenId);
                if (userInfo != null) {
                    friends.add(userInfo);
                }
            }
        }

        return friends;
    }

}
