package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.example.demo.bean.UserDisplay;
import com.example.demo.entity.Account;
import com.example.demo.service.AccountService;
import com.example.demo.utils.SessionConstant;

@Controller
@RequestMapping("/account")
public class AccountController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AccountService accountService;

    @RequestMapping("/list")
    @ResponseBody
    public List<Account> list(HttpServletRequest request) {
        List<Account> findAll = accountService.findAll();
        return findAll;
    }

    @RequestMapping("/page/parent")
    @ResponseBody
    public Object page(String parent, Integer userType, int pageSize, int pageNumber, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (pageNumber != 0) {
            pageNumber = pageSize / pageNumber;
        }
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("addTime"));
        Page<Account> doctorPageByAccount = accountService.findDoctorPageByAccount(parent.trim(), page);
        result.put("total", doctorPageByAccount.getTotalElements());
        result.put("rows", doctorPageByAccount.getContent());
        return result;
    }

    @RequestMapping("/page")
    @ResponseBody
    public Map<String, Object> page(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, int pageSize, int pageNumber,
            HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (pageNumber != 0) {
            pageNumber = pageSize / pageNumber;
        }
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("addTime"));
        if (user != null) {
            Page<Account> pageByUserType = null;
            // admin
            if (user.getUserType() == 0) {
                pageByUserType = accountService.findPageByUserType(1, page);
            } else {
                pageByUserType = accountService.findDoctorPageByAccount(user.getUsername(), page);
            }
            result.put("total", pageByUserType.getTotalElements());
            result.put("rows", pageByUserType.getContent());
        }
        return result;
    }

    @RequestMapping("/save")
    @ResponseBody
    public Map<String, Object> page(@RequestBody Account account, HttpServletRequest request) {
        return accountService.add(account);
    }

    @RequestMapping("/ticket/create")
    @ResponseBody
    public Map<String, Object> page(String account, HttpServletRequest request) {
        return accountService.createTicket(account);
    }

    @RequestMapping("/find")
    @ResponseBody
    public Optional<Account> findById(String account, HttpServletRequest request) {
        return accountService.findById(account);
    }

}
