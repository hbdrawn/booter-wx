package com.example.demo.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.bean.AccessTocken4Url;
import com.example.demo.bean.EventCallBack;
import com.example.demo.bean.UserDisplay;
import com.example.demo.bean.WxUserInfo;
import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.AccountService;
import com.example.demo.utils.Event2Customer;
import com.example.demo.utils.HttpClientPool;
import com.example.demo.utils.SerialUtils;
import com.example.demo.utils.SessionConstant;
import com.example.demo.utils.WxUsersServiceUtils;
import com.example.demo.utils.XMLUtils;

@Controller
@RequestMapping("/event")
public class CallBackCustomerList {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Value("${weixin.app-id}")
    private String appId;

    @Value("${weixin.secret}")
    private String secret;

    @Autowired
    private AccountService accountService;

    /*
     * https://open.weixin.qq.com/connect/oauth2/authorize?appid=
     * wx16df682ddc3c0fa4&redirect_uri=http%3A%2F%2Fwww.idebug.top%2Fevent%
     * 2Flogin&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect
     * private String code2TokenUrl =
     * "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
     */
    public static String code2TokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid=";

    @RequestMapping("/authr")
    @ResponseBody
    public String authr(String msg_signature, String timestamp, String nonce, String echostr, HttpServletRequest request) {
        String params = "";
        try {
            ServletInputStream inputStream = request.getInputStream();
            params = IOUtils.toString(inputStream, "UTF-8");
            if (params.indexOf("subscribe") != -1) {
                EventCallBack bean = XMLUtils.toBean(params, EventCallBack.class);
                logger.debug("收到用户事件:{}", bean);

                if (params.indexOf("unsubscribe") != -1) {
                    // 用户取消关注
                    CustomerList customerList = Event2Customer.convert(bean);
                    List<CustomerList> findOne = customerRepository.findByFromUserNameAndStatus(customerList.getFromUserName(), 1);
                    for (CustomerList tmp : findOne) {
                        tmp.setStatus(2);
                        customerRepository.save(tmp);
                        logger.info("取消微信关注成功:{}", bean);
                    }

                } else if ("subscribe".equals(bean.getEvent()) && !bean.getEventKey().equals("")) {
                    CustomerList customerList = Event2Customer.convert(bean);
                    WxUserInfo userInfo = WxUsersServiceUtils.getUserInfo(customerList.getFromUserName());
                    if (userInfo != null) {
                        CustomerList transfer = userInfo.transfer();
                        transfer.setAccount(customerList.getAccount());
                        transfer.setAddTime(new Date());
                        transfer.setCreateTime(customerList.getCreateTime());
                        transfer.setEvent(customerList.getEvent());
                        transfer.setFromUserName(customerList.getFromUserName());
                        transfer.setMsgType(customerList.getMsgType());
                        transfer.setTicket(customerList.getTicket());
                        transfer.setToUserName(customerList.getToUserName());
                        customerList = transfer;
                    }
                    customerRepository.save(customerList);
                    logger.info("添加微信关注用户成功:{}", bean);
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        logger.debug("回调接口上送信息:{},{},{},{}:\n{}", msg_signature, timestamp, nonce, echostr, params);
        return echostr;
    }

    /*
     * private String userInfoUril =
     * "https://api.weixin.qq.com/sns/userinfo?lang=zh_CN&access_token=ACCESS_TOKEN&openid=OPENID";
     */

    protected String userInfoUril = "https://api.weixin.qq.com/sns/userinfo?lang=zh_CN&access_token=";

    /**
     * . 描述:
     *
     * @author zhangyj
     * @date 2018年7月11日 下午9:43:14
     * @version
     * 
     * @param code
     * @param state
     * @param fromType
     *            0或null代表从个人中心进来的用户；2代表从特医商城进来的用户
     * @param session
     * @return
     */
    @RequestMapping("/login")
    public String wxLogin(String code, String state, Integer fromType, HttpSession session) {
        String url = "redirect:/admin/suc";
        try {
            logger.info("用户跳转个人中心:{},{},{}", code, state, fromType);
            // 通过code换取网页授权access_token
            String invokeGet = HttpClientPool.invokeGet(code2TokenUrl + appId + "&secret=" + secret + "&code=" + code);
            logger.debug("获取accesstoken:{}", invokeGet);
            if (invokeGet != null && invokeGet.indexOf("errcode") == -1) {
                AccessTocken4Url accessTocken = SerialUtils.deserial(invokeGet, AccessTocken4Url.class);
                if (accessTocken != null) {
                    logger.info("code拉取access_token成功:{}", invokeGet);
                    // 拉去用户ID
                    /*
                     * String userInfoStr = HttpClientPool
                     * .invokeGet(userInfoUril + accessTocken.getAccess_token()
                     * + "&openid=" + accessTocken.getOpenid()); if (userInfoStr
                     * != null && userInfoStr.indexOf("errcode") == -1) {}
                     * logger.info("access_tocken拉取用户信息成功:{}", userInfoStr);
                     * SnsapiUserInfo userInfo =
                     * SerialUtils.deserial(userInfoStr, SnsapiUserInfo.class);
                     */
                    Optional<Account> account = accountService.findByOpenId(accessTocken.getOpenid());
                    if (account.isPresent() && account.get().getState() == 1) {
                        UserDisplay display = new UserDisplay();
                        display.setUsername(account.get().getAccount());
                        display.setUserType(account.get().getUserType());
                        display.setWxOpenId(account.get().getOpenId());
                        session.setAttribute(SessionConstant.SESSION_ID, display);
                        if (fromType == null || fromType == 1) {
                            session.setAttribute(SessionConstant.WEIXIN_LOGIN, 1);
                        } else {
                            session.setAttribute(SessionConstant.WEIXIN_LOGIN, 3);
                            url = "redirect:/admin/suc/nologion";
                        }
                    } else {
                        List<CustomerList> findByFromUserNameAndStatus = customerRepository.findByFromUserNameAndStatus(accessTocken.getOpenid(), 1);
                        if (findByFromUserNameAndStatus != null && !findByFromUserNameAndStatus.isEmpty()) {
                            UserDisplay display = new UserDisplay();
                            display.setUsername(account.get().getAccount());
                            display.setUserType(account.get().getUserType());
                            display.setWxOpenId(account.get().getOpenId());
                            session.setAttribute(SessionConstant.SESSION_ID, display);
                            if (fromType == null || fromType == 1) {
                                session.setAttribute(SessionConstant.WEIXIN_LOGIN, 2);
                            } else {
                                session.setAttribute(SessionConstant.WEIXIN_LOGIN, 3);
                            }
                        } else {
                            logger.warn("用户不存在:{}", accessTocken.getOpenid());
                            session.setAttribute(SessionConstant.WEIXIN_LOGIN, 2);
                        }
                        url = "redirect:/admin/suc/nologion";
                    }

                }
            }
        } catch (Exception e) {
            logger.error("用户微信登陆跳转错误:" + code, e);
        }

        return url;
    }

}
