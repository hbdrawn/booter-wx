package com.example.demo.controller;

import java.io.IOException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.bean.EventCallBack;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.utils.Event2Customer;
import com.example.demo.utils.XMLUtils;

/**
 * .描述:
 * 
 * @deprecated
 * 
 * @author zhangyj
 * @date 2018年7月13日 上午8:29:40
 * @version CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Deprecated
@Controller
@RequestMapping("/youzan/callback")
public class CallBackOrderController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CustomerRepository customerRepository;

    /*
     * @RequestMapping("/callback")
     * 
     * @ResponseBody public Map<String, Object> eventCallBack(HttpServletRequest
     * request) { Map<String, Object> result = new HashMap<String, Object>();
     * String params = null; try { ServletInputStream inputStream =
     * request.getInputStream(); params = IOUtils.toString(inputStream,
     * "UTF-8"); EventCallBack bean = XMLUtils.toBean(params,
     * EventCallBack.class); logger.debug("收到用户事件:{}", bean);
     * 
     * if (bean.getEvent().equals("subscribe") &&
     * !bean.getEventKey().equals("")) { CustomerList customerList =
     * Event2Customer.convert(bean); customerRepository.save(customerList);
     * result.put("code", 1); result.put("msg", "success"); }
     * 
     * } catch (IOException e) { logger.error("用户事件处理错误", e); result.put("code",
     * 2); result.put("msg", e.getMessage()); } logger.info("收到用户事件:{}",
     * params);
     * 
     * return result; }
     */

    @RequestMapping("/authr")
    @ResponseBody
    public String authr(String msg_signature, String timestamp, String nonce, String echostr, HttpServletRequest request) {
        String params = "";
        try {
            ServletInputStream inputStream = request.getInputStream();
            params = IOUtils.toString(inputStream, "UTF-8");
            if (params.indexOf("subscribe") != -1) {
                EventCallBack bean = XMLUtils.toBean(params, EventCallBack.class);
                logger.debug("收到用户事件:{}", bean);

                if ("subscribe".equals(bean.getEvent()) && !bean.getEventKey().equals("")) {
                    CustomerList customerList = Event2Customer.convert(bean);
                    customerRepository.save(customerList);
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        logger.debug("回调接口上送信息:{},{},{},{}:\n{}", msg_signature, timestamp, nonce, echostr, params);
        return echostr;
    }

}
