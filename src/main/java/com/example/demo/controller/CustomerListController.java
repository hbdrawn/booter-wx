package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.example.demo.bean.UserDisplay;
import com.example.demo.bean.WxUserInfo;
import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.AccountService;
import com.example.demo.utils.SessionConstant;
import com.example.demo.utils.WxUsersServiceUtils;

@Controller
@RequestMapping("/customer")
public class CustomerListController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    AccountService accountService;

    @RequestMapping("/list")
    @ResponseBody
    public List<CustomerList> list(@RequestBody CustomerList customer, HttpServletRequest request) {
        List<CustomerList> findAll = customerRepository.findAll();
        return findAll;
    }

    @RequestMapping("/list/wx")
    @ResponseBody
    public Object listWx(HttpServletRequest request, int type) {
        List<Account> findAllByType = accountService.findAll();
        Map<String, Account> allUsersMap = new HashMap<>();
        for (Account account : findAllByType) {
            allUsersMap.put(account.getOpenId(), account);
        }

        List<CustomerList> findAll = customerRepository.findAll();
        Map<String, CustomerList> customerUserMap = new HashMap<>();
        for (CustomerList tmp : findAll) {
            customerUserMap.put(tmp.getFromUserName(), tmp);
        }
        List<WxUserInfo> reList = new ArrayList<>();
        List<WxUserInfo> userList = WxUsersServiceUtils.getUserList();
        for (WxUserInfo userInfo : userList) {
            if (allUsersMap.get(userInfo.getOpenid()) != null || customerUserMap.get(userInfo.getOpenid()) != null) {
                continue;
            }
            reList.add(userInfo);
        }
        return reList;
    }

    @RequestMapping("/page")
    @ResponseBody
    public Object page(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, int pageSize, int pageNumber, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (pageNumber != 0) {
            pageNumber = pageSize / pageNumber;
        }
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.DESC, "addTime"));
        Page<CustomerList> findByAccount = customerRepository.findByAccount(user.getUsername(), page);
        result.put("total", findByAccount.getTotalElements());
        result.put("rows", findByAccount.getContent());
        return result;
    }

}
