
package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * . description:
 * 
 * @author zhangyj
 * @date 2018年7月28日 下午5:58:27
 * @version CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Controller
public class IndexController {
    @RequestMapping("/")
    public String index(HttpServletRequest request) {
        return "admin/login_new.html";
    }
}
