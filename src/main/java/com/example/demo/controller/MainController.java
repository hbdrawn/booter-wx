package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.bean.UserDisplay;
import com.example.demo.entity.Account;
import com.example.demo.service.AccountService;
import com.example.demo.utils.SessionConstant;

@Controller
@RequestMapping("/admin")
public class MainController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AccountService accountService;

    @RequestMapping("/index")
    public String index(HttpServletRequest request) {
        return "admin/login_new.html";
    }

    @RequestMapping("/suc")
    public String loginSuc(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user,
            @SessionAttribute(SessionConstant.WEIXIN_LOGIN) Integer wxLogin, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String success = "";
        String monthStr = request.getParameter("monthStr");
        if (user != null) {
            if (user.getUserType() == 0) {
                success += "user/list.html";
            } else if (wxLogin != null) {
                if (monthStr != null) {
                    redirectAttributes.addAttribute("monthStr", monthStr);
                }
                if (wxLogin == 1) {
                    success = "redirect:/m/login";
                } else if (wxLogin == 2) {
                    success = "mobile/devtips.html";
                } else if (wxLogin == 3) {
                    success = "mobile/gotoshop.html";
                }
            } else if (user.getUserType() == 1) {
                success += "doctor/list.html";
            } else if (user.getUserType() == 2) {
                // TODO
                success += "customer/list.html";
            }

        } else {
            success = "admin/login_new.html";
        }
        return success;
    }

    @RequestMapping("/suc/nologion")
    public String nologinSuc(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user,
            @SessionAttribute(SessionConstant.WEIXIN_LOGIN) Integer wxLogin) {
        String success = "";
        if (wxLogin == 1) {
            success = "redirect:/m/login";
        } else if (wxLogin == 2) {
            success = "mobile/devtips.html";
        } else if (wxLogin == 3) {
            success = "mobile/gotoshop.html";
        }
        return success;
    }

    @RequestMapping("/myDoctor")
    public String loginSuc(String parent, HttpServletRequest request, Model model) {
        model.addAttribute("parent", parent);
        return "doctor/list.html";
    }

    @RequestMapping("/orders/list")
    public String ordersList(HttpServletRequest request) {
        return "orders/list.html";
    }

    @RequestMapping("/login")
    @ResponseBody
    public Object login(@RequestBody UserDisplay userDisplay, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        // 超级用户直接调转到销售用户管理列表
        if ("admin".equals(userDisplay.getUsername()) && "root".equals(userDisplay.getPassword())) {
            userDisplay.setPassword(null);
            userDisplay.setUserType(0);
            HttpSession session = request.getSession();
            result.put("code", 1);
            result.put("msg", "登陆成功");
            session.setAttribute(SessionConstant.SESSION_ID, userDisplay);
            session.setAttribute(SessionConstant.WEIXIN_LOGIN, 0);
            return result;
        }

        Optional<Account> findById = accountService.findById(userDisplay.getUsername());
        if (findById.isPresent() && findById.get().getPassword().equals(userDisplay.getPassword())) {
            userDisplay.setPassword(null);
            userDisplay.setUserType(findById.get().getUserType());
            userDisplay.setWxOpenId(findById.get().getOpenId());
            HttpSession session = request.getSession();
            session.setAttribute(SessionConstant.SESSION_ID, userDisplay);
            session.setAttribute(SessionConstant.WEIXIN_LOGIN, 0);
            result.put("code", 1);
            result.put("msg", "登陆成功");
        } else {
            result.put("code", 2);
            result.put("msg", "用户名或密码错误");
        }

        return result;
    }

    @RequestMapping("/logout")
    public Object logout(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, HttpServletRequest request) {
        return "admin/login_new.html";
    }
}
