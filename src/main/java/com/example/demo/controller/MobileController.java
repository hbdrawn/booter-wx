package com.example.demo.controller;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import com.example.demo.bean.OrdersDisplay4Mlogin;
import com.example.demo.bean.UserDisplay;
import com.example.demo.service.Orders4MobileLoginService;
import com.example.demo.utils.SessionConstant;

@Controller
@RequestMapping("/m")
public class MobileController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    Orders4MobileLoginService orders4MobileLoginService;

    @RequestMapping("/login")
    public String index(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, Model model, String monthStr, String doctorName) {
        String url = "redirect:/admin/index";
        OrdersDisplay4Mlogin display = new OrdersDisplay4Mlogin();
        if (monthStr != null) {
            String[] split = monthStr.split("-");
            display.setYear(Integer.valueOf(split[0]));
            display.setMonth(Integer.valueOf(split[1]));
        } else {
            Calendar cal = Calendar.getInstance();
            display.setYear(cal.get(Calendar.YEAR));
            display.setMonth(cal.get(Calendar.MONTH) + 1);
        }
        display.setAccount(user.getUsername());
        if (doctorName != null) {
            display.setAccount(doctorName);
            model.addAttribute("doctorName", doctorName);
        }
        OrdersDisplay4Mlogin ordersDisplay4Mlogin = new OrdersDisplay4Mlogin();
        if (doctorName == null && user.getUserType() == 1) {
            ordersDisplay4Mlogin = orders4MobileLoginService.getOrdersDisplay4Parent(display);
            url = "mobile/xiaoshou.html";
        } else if (doctorName != null || user.getUserType() == 2) {
            ordersDisplay4Mlogin = orders4MobileLoginService.getOrdersDisplay4Doctor(display);
            url = "mobile/yisheng.html";
        }
        model.addAttribute("orders", ordersDisplay4Mlogin);

        return url;
    }

}
