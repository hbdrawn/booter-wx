package com.example.demo.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.example.demo.bean.OrdersDisplay;
import com.example.demo.bean.UserDisplay;
import com.example.demo.entity.Account;
import com.example.demo.entity.Orders;
import com.example.demo.service.AccountService;
import com.example.demo.service.OrdersService;
import com.example.demo.utils.DateUtils;
import com.example.demo.utils.SessionConstant;

@Controller
@RequestMapping("/order")
public class OrdersController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private AccountService accountService;

    @RequestMapping("/total")
    public String total(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, String startDate, String endDate, HttpServletRequest request,
            Model model) {
        Orders orders = new Orders();
        if (user.getUserType() == 1) {
            orders.setParentAccount(user.getUsername());
        } else if (user.getUserType() == 2) {
            orders.setDoctorAccount(user.getUsername());
        }
        Date start = null;
        Calendar cal = Calendar.getInstance();
        if (startDate == null) {
            cal.add(Calendar.MONTH, -1);
            start = cal.getTime();
        } else {
            start = DateUtils.string2Date(startDate, DateUtils.ALL);
        }

        Date end = null;
        if (endDate == null) {
            cal.add(Calendar.MONTH, 1);
            end = cal.getTime();
        } else {
            end = DateUtils.string2Date(endDate, DateUtils.ALL);
        }

        OrdersDisplay ordersDisplay = ordersService.getInfoByDate(orders, start, end);
        model.addAttribute("OrdersDisplay", ordersDisplay);
        // model.addAttribute(SessionConstant.SESSION_ID, user);
        return "doctor/my/orders.html";
    }

    @RequestMapping("/page")
    @ResponseBody
    public Object page(@SessionAttribute(SessionConstant.SESSION_ID) UserDisplay user, int pageSize, int pageNumber, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (user != null) {
            Page<Orders> pageByUserType = null;
            if (pageNumber != 0) {
                pageNumber = pageSize / pageNumber;
            }
            if (user.getUserType() == 1) {
                pageByUserType = ordersService.findPageByParent(user.getUsername(), pageSize, pageNumber);
            } else {
                pageByUserType = ordersService.findPageByDoctor(user.getUsername(), pageSize, pageNumber);
            }
            result.put("total", pageByUserType.getTotalElements());
            result.put("rows", pageByUserType.getContent());
        }
        return result;
    }

    @RequestMapping("/html/page")
    @ResponseBody
    public Object htmlPage(int pageSize, int pageNumber, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (pageNumber != 0) {
            pageNumber = pageSize / pageNumber;
        }
        Page<Orders> findAll = ordersService.findAll(pageSize, pageNumber);
        result.put("total", findAll.getTotalElements());

        List<Account> findAll2 = accountService.findAll();
        Map<String, Account> maps = new HashMap<>();
        for (Account tmp : findAll2) {
            maps.put(tmp.getAccount(), tmp);
        }

        List<Orders> content = findAll.getContent();
        for (Orders tmp : content) {
            Account account = maps.get(tmp.getParentAccount());
            if (account != null) {
                tmp.setParentName(account.getName());
            }

            Account accountDoctor = maps.get(tmp.getDoctorAccount());
            if (accountDoctor != null) {
                tmp.setDoctorName(accountDoctor.getName());
            }
        }
        result.put("rows", content);

        return result;
    }

}
