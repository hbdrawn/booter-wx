package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.utils.WxUsersServiceUtils;

@Controller
@RequestMapping("/test")
public class TestController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping("/html")
    public String index(HttpServletRequest request, Model model) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        model.addAttribute("list", list);
        return "test";
    }

    @RequestMapping("/login")
    public String redirect(HttpServletRequest request, Model model) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        model.addAttribute("list", list);
        return "redirect:/admin/suc";
    }

    @Autowired

    @RequestMapping("/getUserList")
    @ResponseBody
    public Object getUserList(HttpServletRequest request) {
        return WxUsersServiceUtils.getUserList();
    }

}
