package com.example.demo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.springframework.stereotype.Component;

import com.example.demo.bean.AccessTocken4Url;
import com.example.demo.cache.MsgCache;
import com.example.demo.cache.RTAccountCache;
import com.example.demo.utils.AESUtils;
import com.example.demo.utils.JsonUtils;
import com.example.demo.utils.WxUsersServiceUtils;
import com.example.realtime.MsgBody;
import com.example.realtime.MsgWrapper;
import com.example.realtime.utils.ConversationsType;
import com.example.realtime.utils.UserInfo;
import com.example.framework.config.WebSocketConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ServerEndpoint(value = "/websocket", configurator = WebSocketConfig.class)
@Component
@Slf4j
@Data
public class WebSocketServer {

    // 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    // concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static Map<String, WebSocketServer> webSocketSet = new ConcurrentHashMap<>();

    // 与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    private UserInfo userInfo;
    private String openId;
    private AccessTocken4Url accessTocken4Url;
    /**
     * openId
     */
    private List<String> friends = new ArrayList<>();

    /**
     * .连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        // webSocketSet.add(this); // 加入set中
        addOnlineCount(); // 在线数加1
        log.info("有新窗口开始监听,当前在线人数为" + getOnlineCount());
    }

    /**
     * .连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if (openId != null) {
            webSocketSet.remove(openId); // 从set中删除
        }
        subOnlineCount(); // 在线数减1
        log.info("有一连接关闭！当前在线人数为" + getOnlineCount());
    }

    /**
     * .收到客户端消息后调用的方法
     *
     * @param message
     *            .客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("收到来自窗口[{}]的消息:{}", openId, message);
        MsgWrapper deserial = JsonUtils.deserial(message, MsgWrapper.class);
        if (deserial.getType() == ConversationsType.get_login) {
            String encryptData = deserial.getContent();
            String iv = deserial.getIv();
            accessTocken4Url = WxUsersServiceUtils.getOpenId(deserial.getCode());
            if (accessTocken4Url.getUnionid() == null) {
                // 获取unionId
                String decrypt = AESUtils.decrypt(accessTocken4Url.getSession_key(), iv, encryptData);
                @SuppressWarnings("unchecked")
                Map<String, Object> result = JsonUtils.deserial(decrypt, Map.class);
                if (result.get("unionId") != null) {
                    String string = RTAccountCache.unionIdWithOpenIdMap.get((String) result.get("unionId"));
                    if (string != null) {
                        openId = string;
                    }
                }

            }
            // openId = "oKhLb0R_1oeSWc9AHNX2GFoOFR_Q";
            if (openId == null) {
                log.warn("获取公众号ID失败[{}]", message);
                return;
            }
            // openId = deserial.getCode();
            userInfo = RTAccountCache.getUserInfo(openId);
            if (userInfo != null) {
                webSocketSet.put(openId, this);
                log.info("用户登陆成功[{}]", userInfo);
            } else {
                log.warn("用户登陆失败[{}]", openId);
            }
            Map<String, Object> result = new HashMap<>();
            result.put("type", deserial.getType());
            result.put("userInfo", userInfo);
            sendMessage(JsonUtils.serial(result));
            return;
        } else if (deserial.getType() == ConversationsType.get_conversations) {
            // 拉取会话列表
            Map<String, List<MsgWrapper>> list = MsgCache.msgCache.get(openId);
            if (list != null && !list.isEmpty()) {
                Map<String, Object> result = new HashMap<>();
                List<MsgWrapper> newList = new ArrayList<>();
                for (String key : list.keySet()) {
                    List<MsgWrapper> list2 = list.get(key);
                    MsgWrapper msgWrapper = list2.get(list2.size() - 1);
                    // MsgWrapper clone = (MsgWrapper) msgWrapper.clone();
                    newList.add(msgWrapper);
                }

                // for(MsgWrapper wrapper : newList) {
                // wrapper.setContent(null);
                // }
                result.put("conversations", newList);
                result.put("type", deserial.getType());
                sendMessage(JsonUtils.serial(result));
                log.info("用户拉取会话列表成功[{}],[{}]", newList.size(), userInfo);
            }
            return;

        } else if (deserial.getType() == ConversationsType.get_history) {
            // 拉取历史会话消息
            Map<String, List<MsgWrapper>> list = MsgCache.msgCache.get(openId);
            if (list != null && !list.isEmpty()) {
                List<MsgWrapper> list2 = list.get(deserial.getFriendId());
                if (list2 != null && !list2.isEmpty()) {
                    for (MsgWrapper msgWrapper : list2) {
                        sendMessage(JsonUtils.serial(msgWrapper));
                        msgWrapper.setUnread(0);
                    }
                    log.info("用户拉取历史会话列表[{}]成功[{}]", list2.size(), userInfo.getNickName());
                }
            }
            return;
        } else if (deserial.getType() == ConversationsType.get_friends) {
            // 获取朋友列表
            List<UserInfo> friends = RTAccountCache.getFriends(openId);
            if (friends != null && !friends.isEmpty()) {
                Map<String, Object> result = new HashMap<>();
                result.put("friends", friends);
                result.put("type", deserial.getType());
                sendMessage(JsonUtils.serial(result));
                log.info("用户拉取朋友列表成功[{}],[{}]", friends.size(), userInfo.getNickName());
            }
            return;
        }

        if (openId != null) {
            // 实时消息处理
            deserial.setTimestamp(new Date());
            UserInfo friendInfo = RTAccountCache.getUserInfo(deserial.getFriendId());
            deserial.setFriendHeadUrl(friendInfo.getMyHeadUrl());
            deserial.setFriendName(friendInfo.getNickName());
            MsgBody msgBody = new MsgBody();
            msgBody.setContent(deserial.getContent());
            msgBody.setType(deserial.getType().name());
            String serial = JsonUtils.serial(msgBody);
            deserial.setLatestMsg(serial.replaceAll("/", ""));
            deserial.setMsgUserId(openId);
            deserial.setMsgUserHeadUrl(userInfo.getMyHeadUrl());
            deserial.setMsgUserName(userInfo.getNickName());

            WebSocketServer webSocketServer = webSocketSet.get(deserial.getFriendId());
            if (webSocketServer != null) {
                webSocketServer.sendMessage(JsonUtils.serial(deserial));
            }

            MsgCache.put(deserial.getFriendId(), openId, deserial, true);
            MsgCache.put(openId, deserial.getFriendId(), deserial, false);
        }

    }

    /**
     * 
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误", error);
    }

    /**
     * . 实现服务器主动推送
     */
    public void sendMessage(String message) {
        try {
            this.session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            log.error("下发消息失败[" + message + "]", e);
        }
    }

    /**
     * . 群发自定义消息
     */
    public static void sendInfo(String message, String openId) throws IOException {
        log.info("推送消息到窗口[{}],推送内容:[{}]", openId, message);
        for (WebSocketServer item : webSocketSet.values()) {
            // 这里可以设定只推送给这个sid的，为null则全部推送
            if (item.openId == null) {
                item.sendMessage(message);
            } else if (item.openId.equals(openId)) {
                item.sendMessage(message);
            }
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}