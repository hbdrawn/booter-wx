package com.example.demo.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
public class Account {

    // @GeneratedValue
    /**
     * .目前配合微信公众号做递增1-100000
     */
    /** private Short id; **/

    /**
     * . 此字段唯一，即作为医生用户的用户名，也作为获取医生二维码的scene_str场景值
     */
    @Id
    private String account;
    private String phone;
    private String name;
    @Column(name = "nick_name")
    private String nickname;
    @Column(name = "open_id")
    private String openId;
    @Column(name = "union_id")
    private String unionId;
    private String password;
    private String salt;

    /**
     * 1=销售人员 2=医生
     */
    @Column(name = "type")
    private int userType;
    @Column(name = "create_user")
    private String createUser;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "add_time")
    private Date addTime;
    /**
     * 1=正常 2=禁用
     */
    private int state = 1;

    /**
     * . 微信返回的三个字段信息
     */
    private String ticket;
    private String url;
    private String expire;// seconds

    /**
     * . 医生对应的父类条件:如销售的用户名
     */
    private String parent;

    private String headimgurl;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "account")
    private Set<CustomerList> customerList;
}
