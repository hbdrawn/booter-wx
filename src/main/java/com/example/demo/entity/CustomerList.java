package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity(name = "customer_list")
public class CustomerList {
    @Id
    @GeneratedValue
    private Long id;

    /*
     * . 場景ID,即用户表的account,EventKey,事件KEY值，qrscene_为前缀，后面为二维码的参数值
     */
    private String account;
    @Column(name = "from_user_name")
    private String fromUserName;
    @Column(name = "to_user_name")
    private String toUserName;
    private String event;
    private String ticket;
    @Column(name = "type")
    private String msgType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "create_time")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "add_time")
    private Date addTime;

    private Integer subscribe;
    /* private String openid; */
    private String nickname;
    /*
     * . 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;
    private String language;
    private String city;
    private String province;
    private String country;
    private String headimgurl;
    private Long subscribe_time;
    private String unionid;
    private String remark;
    private String groupid;
    private String tagid_list;
    /**
     * . 返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION
     * .公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE
     * .扫描二维码，ADD_SCENEPROFILE LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM
     * .图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他
     */
    private String subscribe_scene;
    private Long qr_scene;
    private String qr_scene_str;
    /*
     * 1=正常 2=已取消关注
     */
    private Integer status = 1;
}
