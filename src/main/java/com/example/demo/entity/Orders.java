package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
public class Orders {

    // @GeneratedValue
    // private Long id;

    /**
     * . 交易编号
     */
    @Id
    private String tid;
    /**
     * . 收货人的所在城市。<br>
     * PS：如果订单类型是送礼订单，收货地址在sub_trades字段中；如果物流方式是到店自提，收货地址在fetch_detail字段中
     */
    @Column(name = "receiver_city")
    private String receiverCity;
    /**
     * 收货人的所在地区
     */
    @Column(name = "receiver_district")
    private String receiverDistrict;
    /**
     * . 收货人的姓名
     */
    @Column(name = "receiver_name")
    private String receiverName;
    /**
     * .收货人的所在省份
     */
    @Column(name = "receiver_state")
    private String receiverState;
    /**
     * .收货人的手机号码
     */
    @Column(name = "receiver_mobile")
    private String receiverMobile;

    /**
     * .商品数字编号。当一个trade对应多个order的时候，值为第一个交易明细中的商品的编号
     */
    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "pic_thumb_path")
    private String picThumbPath;

    private String title;
    private Long num;
    /**
     * .退款状态。取值范围：<br>
     * NO_REFUND（无退款）<br>
     * PARTIAL_REFUNDING（部分退款中）<br>
     * PARTIAL_REFUNDED（已部分退款）<br>
     * PARTIAL_REFUND_FAILED（部分退款失败）<br>
     * FULL_REFUNDING（全额退款中）<br>
     * FULL_REFUNDED（已全额退款）<br>
     * FULL_REFUND_FAILED（全额退款失败）<br>
     */
    @Column(name = "refund_state")
    private String refundState;

    /**
     * .交易状态。取值范围：<br>
     * TRADE_NO_CREATE_PAY (没有创建支付交易) <br>
     * WAIT_BUYER_PAY (等待买家付款)<br>
     * WAIT_PAY_RETURN (等待支付确认)<br>
     * WAIT_GROUP（等待成团，即：买家已付款，等待成团）<br>
     * WAIT_SELLER_SEND_GOODS (等待卖家发货，即：买家已付款) <br>
     * WAIT_BUYER_CONFIRM_GOODS (等待买家确认收货，即：卖家已发货) <br>
     * TRADE_BUYER_SIGNED (买家已签收) <br>
     * TRADE_CLOSED (付款以后用户退款成功，交易自动关)<br>
     * TRADE_INVALID （订单无效，当前状态无需处理）<br>
     */
    private String status;

    private String statusStr;

    /**
     * .商品总价（商品价格乘以数量的总金额）。单位：元，精确到分
     */
    @Column(name = "total_fee")
    private Float totalFee;

    @Column(name = "payment")
    private Float payment;

    /**
     * .交易创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date created;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "update_time")
    private Date updateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "pay_time")
    private Date payTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "add_time")
    private Date addTime;

    /**
     * .粉丝昵称
     * 订单信息中存在三方(例如微信)粉丝昵称则取粉丝昵称;取不到粉丝昵称时则使用买家手机号;以上两点未满足时取买家收货人信息;无收货人信息时返回[匿名]
     */
    @Column(name = "fans_nickname")
    private String fansNickname;
    /**
     * 粉丝id
     */
    @Column(name = "fans_id")
    private Long fansId;
    /**
     * 有赞买家ID
     */
    @Column(name = "buyer_id")
    private Long buyerId;
    /**
     * 0:未知、1:微信自有粉丝
     */
    @Column(name = "fans_type")
    private Long fansType;
    /**
     * 微信openid
     */
    @Column(name = "fans_weixin_openid")
    private String fansWeixinOpenid;

    /**
     * . 微信openid
     * 
     * 销售用户
     */
    @Column(name = "parent_account")
    private String parentAccount;

    @Transient
    private String parentName;
    @Transient
    private String doctorName;

    /**
     * .微信openid
     * 
     * doctor
     */
    @Column(name = "doctor_account")
    private String doctorAccount;

    /**
     * .纬度
     */
    @Column(name = "lat")
    private String lat;
    /**
     * .经度
     */
    @Column(name = "lng")
    private String lng;
}
