package com.example.demo.repository;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Account;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AcountRepository extends JpaRepository<Account, String> {

    List<Account> findByUserType(int userType);

    Page<Account> findPageByUserType(int userType, Pageable pageable);

    Page<Account> findPageByParent(String parent, Pageable pageable);

    Optional<Account> findByNickname(String nickname);

    Optional<Account> findByOpenId(String openId);

    List<Account> findAllByParent(String parent);

    List<Account> findAllByUserType(int userType);

}
