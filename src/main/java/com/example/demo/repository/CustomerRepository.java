package com.example.demo.repository;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.CustomerList;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerList, Long> {

    Page<CustomerList> findByAccount(String account, Pageable pageable);

    List<CustomerList> findByFromUserNameAndStatus(String fromUserName, Integer status);

    List<CustomerList> findAllByAccountAndStatus(String account, Integer status);

    List<CustomerList> findAllByStatus(Integer status);

}
