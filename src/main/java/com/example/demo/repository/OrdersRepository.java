package com.example.demo.repository;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Orders;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long>, JpaSpecificationExecutor<Orders> {

    Page<Orders> findPageByDoctorAccount(String doctorAccount, Pageable page);

    Page<Orders> findPageByParentAccount(String parentAccount, Pageable page);

}
