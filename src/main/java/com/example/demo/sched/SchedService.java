package com.example.demo.sched;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.demo.bean.AccessTocken;
import com.example.demo.service.YouZanGetOrderService;
import com.example.demo.utils.HttpClientPool;
import com.example.demo.utils.SerialUtils;
import com.example.demo.utils.SysConstant;

@Component
public class SchedService {

    private Logger logger = LoggerFactory.getLogger(SchedService.class);

    @Value("${weixin.access-token-url}")
    private String accessTokenUrl;

    @Value("${weixin.app-id}")
    private String appId;

    @Value("${weixin.secret}")
    private String secret;

    @Autowired
    private YouZanGetOrderService youZanGetOrderService;

    @Scheduled(fixedDelay = 7100000)
    public void getAccessTocken() {
        logger.debug("开始拉去token服务");
        String invokeGet = HttpClientPool.invokeGet(accessTokenUrl + "&appid=" + appId + "&secret=" + secret);
        if (invokeGet != null) {
            if (invokeGet.indexOf("errcode") == -1) {
                AccessTocken accessTocken = SerialUtils.deserial(invokeGet, AccessTocken.class);
                if (accessTocken != null) {
                    SysConstant.token = accessTocken.getAccess_token();
                    logger.info("access_tocken更新成功");
                }
            }
        }
    }

//    @Scheduled(cron = "0 0/5 * * * ?")
    public void getOrders() {
        logger.debug("获取订单数据");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, -1);
        // cal.add(Calendar.MINUTE, -6);
        youZanGetOrderService.getOrderList(cal.getTime());
    }
}
