package com.example.demo.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.AcountRepository;
import com.example.demo.utils.QrcodeCreateUtils;

@Service
public class AccountService {

    @Autowired
    private AcountRepository accountRepository;

    public Map<String, Object> add(Account user) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (accountRepository.existsById(user.getAccount())) {
            result.put("code", 3);
            result.put("msg", "用户名[" + user.getAccount() + "]已存在");
            return result;
        } else if (user.getUserType() == 2) {
            if (user.getParent() != null && !user.getParent().equals("")) {
                Optional<Account> findById = findById(user.getParent());
                if (findById.isPresent()) {
                    // 2类用户需要自动创建该用户的二维码信息
                    QrcodeCreateUtils.create(user);
                    /**
                     * result.put("msg", QrcodeCreate.SHOW_URL +
                     * user.getTicket());
                     */
                    result.put("msg", "用户[" + user.getAccount() + "]创建成功");
                    result.put("code", 1);
                } else {
                    result.put("code", 3);
                    result.put("msg", "用户名[" + user.getAccount() + "]分组信息[" + user.getParent() + "]不正确");
                    return result;
                }
            } else {
                result.put("code", 3);
                result.put("msg", "用户名[" + user.getAccount() + "]分组信息不正确");
                return result;
            }
        } else if (user.getUserType() == 1) {
            result.put("code", 1);
            result.put("msg", "用户创建成功");
        }
        user.setAddTime(new Date());
        user.setCreateUser("admin");
        accountRepository.save(user);
        return result;
    }

    public void deleteById(String account) {
        accountRepository.deleteById(account);
    }

    public Page<Account> findPageByUserType(int userType, Pageable pageable) {
        return accountRepository.findPageByUserType(userType, pageable);
    }

    public Page<Account> findDoctorPageByAccount(String parent, Pageable pageable) {
        return accountRepository.findPageByParent(parent, pageable);
    }

    public List<Account> findAllByType(int type) {
        return accountRepository.findAllByUserType(type);
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Map<String, Account> findAllByParent(String parent) {
        Map<String, Account> maps = new HashMap<>();
        List<Account> findAllByParent = accountRepository.findAllByParent(parent);
        for (Account tmp : findAllByParent) {
            maps.put(tmp.getAccount(), tmp);
        }
        return maps;
    }

    public Optional<Account> findById(String account) {
        return accountRepository.findById(account);
    }

    public Optional<Account> findByNickname(String nickname) {
        return accountRepository.findByNickname(nickname);
    }

    public Optional<Account> findByOpenId(String openId) {
        return accountRepository.findByOpenId(openId);
    }

    public Map<String, Object> createTicket(String account) {
        Map<String, Object> result = new HashMap<String, Object>();
        Optional<Account> findById = accountRepository.findById(account);
        if (findById.isPresent()) {
            Account user = findById.get();
            if (user.getUserType() == 2) {
                QrcodeCreateUtils.create(user);
                if (user.getTicket() != null && !user.getTicket().trim().equals("")) {
                    accountRepository.save(user);
                    result.put("code", 1);
                    result.put("msg", QrcodeCreateUtils.SHOW_URL + user.getTicket());
                } else {
                    result.put("code", 2);
                    result.put("msg", "创建失败，请重新生成");
                }
            }
        } else {
            result.put("code", 3);
            result.put("msg", "用户不存在");
        }
        return result;
    }

    public Map<String, Account> getWxUsersList() {
        Map<String, Account> allMaps = new HashMap<String, Account>();
        List<Account> findAll = accountRepository.findAll();
        for (Account tmp : findAll) {
            Set<CustomerList> customerList = tmp.getCustomerList();
            if (customerList != null) {
                for (CustomerList ctmp : customerList) {
                    if (ctmp.getStatus() == 1) {
                        allMaps.put(ctmp.getFromUserName(), tmp);
                    }
                }
            }
        }
        return allMaps;
    }

}
