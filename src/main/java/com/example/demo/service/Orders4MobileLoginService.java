package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.bean.OrdersDisplay;
import com.example.demo.bean.OrdersDisplay4Mlogin;
import com.example.demo.bean.SubDoctorDisplay;
import com.example.demo.bean.SubOrdersDisplay;
import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.entity.Orders;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.utils.DateUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Orders4MobileLoginService {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerRepository customerRepository;

    // 医生用户登陆后看到的所有信息
    public OrdersDisplay4Mlogin getOrdersDisplay4Parent(OrdersDisplay4Mlogin display) {
        OrdersDisplay4Mlogin ordersDisplay4Parent = new OrdersDisplay4Mlogin();
        ordersDisplay4Parent.setYear(display.getYear());
        ordersDisplay4Parent.setMonth(display.getMonth());
        Map<String, Date> currentMonth = DateUtils.getCurrentMonth(display.getMonth(), display.getYear(), DateUtils.ALL);
        Orders condition = new Orders();
        condition.setParentAccount(display.getAccount());
        OrdersDisplay infoByDate = ordersService.getInfoByDate(condition, currentMonth.get("stime"), currentMonth.get("etime"));
        List<Orders> ordersList = infoByDate.getOrdersList();

        Map<String, Account> findAllByParent = accountService.findAllByParent(display.getAccount());
        ordersDisplay4Parent.setSubAccount(findAllByParent.size());
        log.info("销售人员[{}]登陆统计信息:月份[{}],医生总数[{}],订单总数[{}]", display.getAccount(), currentMonth.get("stime"), findAllByParent.size(),
                infoByDate.getOrderNums());
        Map<String, SubDoctorDisplay> doctorMaps = new HashMap<>();
        Map<Long, SubOrdersDisplay> ordersMaps = new HashMap<>();
        for (Orders orders : ordersList) {
            // TODO测试期间可以注释 只要求下面三种状态即可进入销售总额计算
            // * WAIT_SELLER_SEND_GOODS (等待卖家发货，即：买家已付款) <br>
            // * WAIT_BUYER_CONFIRM_GOODS (等待买家确认收货，即：卖家已发货) <br>
            // * TRADE_BUYER_SIGNED (买家已签收) <br>
            if (!(orders.getStatus().equals("WAIT_SELLER_SEND_GOODS") || orders.getStatus().equals("WAIT_BUYER_CONFIRM_GOODS")
                    || orders.getStatus().equals("TRADE_BUYER_SIGNED"))) {
                continue;
            }

            // 以医生用户做分组计算
            SubDoctorDisplay subDoctorDisplay = doctorMaps.get(orders.getDoctorAccount());
            if (subDoctorDisplay == null) {
                subDoctorDisplay = new SubDoctorDisplay();
                Account account = findAllByParent.get(orders.getDoctorAccount());
                subDoctorDisplay.setAccount(orders.getDoctorAccount());
                subDoctorDisplay.setHeadImgUrl(account.getHeadimgurl());
                subDoctorDisplay.setNickName(account.getNickname());
                subDoctorDisplay.setPhone(account.getPhone());
                doctorMaps.put(orders.getDoctorAccount(), subDoctorDisplay);
                findAllByParent.remove(orders.getDoctorAccount());
            }
            subDoctorDisplay.setTotalFee(subDoctorDisplay.getTotalFee() + orders.getPayment());
            // 以产品ID做分组计算
            SubOrdersDisplay subOrdersDisplay = ordersMaps.get(orders.getItemId());
            if (subOrdersDisplay == null) {
                subOrdersDisplay = new SubOrdersDisplay();
                subOrdersDisplay.setItemId(orders.getItemId());
                subOrdersDisplay.setPicThumbPath(orders.getPicThumbPath());
                String title = orders.getTitle();
                int index = title.indexOf("【");
                int last = title.indexOf("】");
                if (index != -1 && last != -1) {
                    title = title.substring(index + 1, last);
                }
                subOrdersDisplay.setTitle(title);
                ordersMaps.put(orders.getItemId(), subOrdersDisplay);
            }
            subOrdersDisplay.setTotalFee(subOrdersDisplay.getTotalFee() + orders.getPayment());

            ordersDisplay4Parent.setTotalFee(ordersDisplay4Parent.getTotalFee() + orders.getPayment());

        }

        for (String tmp : findAllByParent.keySet()) {
            SubDoctorDisplay subDoctorDisplay = new SubDoctorDisplay();
            Account account = findAllByParent.get(tmp);
            subDoctorDisplay.setAccount(tmp);
            subDoctorDisplay.setHeadImgUrl(account.getHeadimgurl());
            subDoctorDisplay.setNickName(account.getNickname());
            subDoctorDisplay.setPhone(account.getPhone());
            doctorMaps.put(tmp, subDoctorDisplay);
        }

        ordersDisplay4Parent.getSubDoctorList().addAll(doctorMaps.values());
        ordersDisplay4Parent.getSubOrdersList().addAll(ordersMaps.values());
        ordersDisplay4Parent.setOrderNums(infoByDate.getOrderNums());
        ordersDisplay4Parent.setAccount(display.getAccount());
        return ordersDisplay4Parent;
    }

    // 医生用户登陆后看到的所有信息
    public OrdersDisplay4Mlogin getOrdersDisplay4Doctor(OrdersDisplay4Mlogin display) {
        OrdersDisplay4Mlogin ordersDisplay4Doctor = new OrdersDisplay4Mlogin();
        ordersDisplay4Doctor.setYear(display.getYear());
        ordersDisplay4Doctor.setMonth(display.getMonth());
        Map<String, Date> currentMonth = DateUtils.getCurrentMonth(display.getMonth(), display.getYear(), DateUtils.ALL);
        Orders condition = new Orders();
        condition.setDoctorAccount(display.getAccount());
        OrdersDisplay infoByDate = ordersService.getInfoByDate(condition, currentMonth.get("stime"), currentMonth.get("etime"));
        List<Orders> ordersList = infoByDate.getOrdersList();
        List<Orders> ordersListNew = new ArrayList<>();
        List<CustomerList> accountAndStatus = customerRepository.findAllByAccountAndStatus(display.getAccount(), 1);
        ordersDisplay4Doctor.setSubAccount(accountAndStatus.size());
        log.info("医生[{}]登陆统计信息:月份[{}],关联患者总数[{}],订单总数[{}]", display.getAccount(), currentMonth.get("stime"), accountAndStatus.size(),
                infoByDate.getOrderNums());
        Map<Long, SubOrdersDisplay> ordersMaps = new HashMap<>();
        for (Orders orders : ordersList) {
            // TODO测试期间可以注释 只要求下面三种状态即可进入销售总额计算
            // * WAIT_SELLER_SEND_GOODS (等待卖家发货，即：买家已付款) <br>
            // * WAIT_BUYER_CONFIRM_GOODS (等待买家确认收货，即：卖家已发货) <br>
            // * TRADE_BUYER_SIGNED (买家已签收) <br>
            if (!(orders.getStatus().equals("WAIT_SELLER_SEND_GOODS") || orders.getStatus().equals("WAIT_BUYER_CONFIRM_GOODS")
                    || orders.getStatus().equals("TRADE_BUYER_SIGNED"))) {
                continue;
            }

            ordersListNew.add(orders);

            // 以产品ID做分组计算
            SubOrdersDisplay subOrdersDisplay = ordersMaps.get(orders.getItemId());
            if (subOrdersDisplay == null) {
                subOrdersDisplay = new SubOrdersDisplay();
                subOrdersDisplay.setItemId(orders.getItemId());
                subOrdersDisplay.setPicThumbPath(orders.getPicThumbPath());
                String title = orders.getTitle();
                int index = title.indexOf("【");
                int last = title.indexOf("】");
                if (index != -1 && last != -1) {
                    title = title.substring(index + 1, last);
                }
                subOrdersDisplay.setTitle(title);
                ordersMaps.put(orders.getItemId(), subOrdersDisplay);
            }
            subOrdersDisplay.setTotalFee(subOrdersDisplay.getTotalFee() + orders.getPayment());

            ordersDisplay4Doctor.setTotalFee(ordersDisplay4Doctor.getTotalFee() + orders.getPayment());

        }
        ordersDisplay4Doctor.setOrders(ordersListNew);
        ordersDisplay4Doctor.getSubOrdersList().addAll(ordersMaps.values());
        ordersDisplay4Doctor.setOrderNums(infoByDate.getOrderNums());
        ordersDisplay4Doctor.setAccount(display.getAccount());
        return ordersDisplay4Doctor;
    }
}
