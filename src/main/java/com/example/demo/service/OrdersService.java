package com.example.demo.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.example.demo.bean.OrdersDisplay;
import com.example.demo.entity.Orders;
import com.example.demo.repository.OrdersRepository;

@Service
public class OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    public Page<Orders> findPageByParent(String parent, Integer pageSize, Integer pageNumber) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.DESC, "addTime"));
        return ordersRepository.findPageByParentAccount(parent, page);
    }

    public Page<Orders> findPageByDoctor(String doctor, Integer pageSize, Integer pageNumber) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.DESC, "addTime"));
        return ordersRepository.findPageByDoctorAccount(doctor, page);
    }

    public OrdersDisplay getInfoByDate(Orders orders, Date start, Date end) {
        OrdersDisplay odisplay = new OrdersDisplay();
        List<Orders> findAll = ordersRepository.findAll(new Specification<Orders>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (orders.getParentAccount() != null) {
                    Path<String> parentAccount = root.get("parentAccount");
                    predicate = criteriaBuilder.and(criteriaBuilder.equal(parentAccount, orders.getParentAccount()));
                } else if (orders.getDoctorAccount() != null) {
                    Path<String> doctorAccount = root.get("doctorAccount");
                    predicate = criteriaBuilder.and(criteriaBuilder.equal(doctorAccount, orders.getDoctorAccount()));
                }

                if (orders.getStatus() != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("status"), orders.getStatus()));
                }

                Path<Date> exp2 = root.get("created");
                query.where(criteriaBuilder.and(predicate, criteriaBuilder.between(exp2, start, end)));
                query.orderBy(criteriaBuilder.desc(root.get("updateTime")));
                return query.getRestriction();
            }
        });

        for (Orders tmp : findAll) {
            // odisplay.setTotalFee(odisplay.getTotalFee() + tmp.getTotalFee());
            odisplay.setTotalFee(odisplay.getTotalFee() + tmp.getPayment());
        }
        odisplay.setOrderNums(findAll.size());
        odisplay.setOrdersList(findAll);
        return odisplay;
    }

    public Map<String, Orders> findAllByDate(Date start, Date end) {
        List<Orders> findAll = ordersRepository.findAll(new Specification<Orders>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Path<Date> exp2 = root.get("addTime");
                return criteriaBuilder.between(exp2, start, end);
            }
        });

        Map<String, Orders> maps = new HashMap<>();
        for (Orders tmp : findAll) {
            maps.put(tmp.getTid(), tmp);
        }

        return maps;
    }

    public Map<String, Orders> findAllByDateWithGreater(Date start) {
        List<Orders> findAll = ordersRepository.findAll(new Specification<Orders>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Path<Date> exp2 = root.get("created");
                return criteriaBuilder.greaterThanOrEqualTo(exp2, start);
            }
        });

        Map<String, Orders> maps = new HashMap<>();
        for (Orders tmp : findAll) {
            maps.put(tmp.getTid(), tmp);
        }

        return maps;
    }

    public Page<Orders> findAll(int pageNumber, int pageSize) {
        Pageable page = PageRequest.of(pageSize, pageNumber, Sort.by(Sort.Direction.DESC, "created"));
        // Pageable page = PageRequest.of(pageSize, pageNumber,
        // Sort.by(Sort.Direction.DESC, "created"));
        return ordersRepository.findAll(page);
    }
}
