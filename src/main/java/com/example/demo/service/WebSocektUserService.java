
package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Account;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.AcountRepository;
import com.example.demo.repository.CustomerRepository;
import com.example.realtime.utils.UserInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年9月8日 下午2:45:20
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Service
@Slf4j
public class WebSocektUserService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AcountRepository accountRepository;

    public UserInfo getUserInfo(String openId) {
        UserInfo userInfo = null;
        Optional<Account> findByOpenId = accountRepository.findByOpenId(openId);
        String userType = "销售用户";
        if (!findByOpenId.isPresent()) {
            List<CustomerList> fromUserNameAndStatus = customerRepository.findByFromUserNameAndStatus(openId, 1);
            if (fromUserNameAndStatus != null && !fromUserNameAndStatus.isEmpty()) {
                if (fromUserNameAndStatus.size() > 1) {
                    log.warn("用户账户[{}]异常，存在多个有效状态关联", openId);
                }
                userType = "关注用户";
                userInfo = new UserInfo();
                userInfo.setUserId(openId);
                userInfo.setNickName(fromUserNameAndStatus.get(0).getNickname());
                userInfo.setMyHeadUrl(fromUserNameAndStatus.get(0).getHeadimgurl());
            }
        }else {
            userInfo = new UserInfo();
            userInfo.setUserId(openId);
            userInfo.setNickName(findByOpenId.get().getNickname());
            userInfo.setMyHeadUrl(findByOpenId.get().getHeadimgurl());
        }
        if(userInfo != null) {
            log.info("{}[{}|{}]登陆聊天系统",userType,userInfo.getUserId(),userInfo.getNickName());
        }
        return userInfo;
    }
}
