package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Account;
import com.example.demo.entity.Orders;
import com.example.demo.repository.OrdersRepository;
import com.youzan.open.sdk.client.auth.Token;
import com.youzan.open.sdk.client.core.DefaultYZClient;
import com.youzan.open.sdk.client.core.YZClient;
import com.youzan.open.sdk.client.oauth.OAuth;
import com.youzan.open.sdk.client.oauth.OAuthContext;
import com.youzan.open.sdk.client.oauth.OAuthFactory;
import com.youzan.open.sdk.client.oauth.OAuthType;
import com.youzan.open.sdk.gen.v3_0_0.api.YouzanTradesSoldGet;
import com.youzan.open.sdk.gen.v3_0_0.model.YouzanTradesSoldGetParams;
import com.youzan.open.sdk.gen.v3_0_0.model.YouzanTradesSoldGetResult;
import com.youzan.open.sdk.gen.v3_0_0.model.YouzanTradesSoldGetResult.FansInfo;
import com.youzan.open.sdk.gen.v3_0_0.model.YouzanTradesSoldGetResult.TradeDetailV2;
import lombok.extern.slf4j.Slf4j;

/**
 * .描述:订单同步接口
 * 
 * @author zhangyj
 * @date 2018年7月25日 下午8:21:15
 * @version CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Service
@Slf4j
public class YouZanGetOrderService {

    @Value("${youzan.client_id}")
    private String clientId;

    @Value("${youzan.client_secret}")
    private String clientSecret;

    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private OrdersService ordersService;

    @Autowired
    private AccountService accountService;

    /**
     * .描述:根据时间点拉取固定时间长度的订单信息
     *
     * @author zhangyj
     * @date 2018年7月25日 下午8:21:44
     * @version
     * 
     * @param cal
     */
    public void getOrderList(Date cal) {
        OAuth oauth = OAuthFactory.create(OAuthType.SELF, new OAuthContext(clientId, clientSecret, 40747450L));
        // System.out.println(JsonUtils.toJson(oauth.getToken().getAccessToken()));

        @SuppressWarnings("resource")
        YZClient client = new DefaultYZClient(new Token(oauth.getToken().getAccessToken())); // new
                                                                                             // Sign(appKey,
                                                                                             // appSecret)
        // YZClient client = new DefaultYZClient(new Sign(clientId,
        // clientSecret));
        YouzanTradesSoldGetParams youzanTradesSoldGetParams = new YouzanTradesSoldGetParams();

        youzanTradesSoldGetParams.setPageSize(100L);
        youzanTradesSoldGetParams.setPageNo(1L);
        try {
            youzanTradesSoldGetParams.setStartUpdate(cal);
        } catch (Exception e) {
        }
        // youzanTradesSoldGetParams.setEndUpdate("2018-05-25 16:31:11");
        // youzanTradesSoldGetParams.setStatus("TRADE_SUCCESS");
        // youzanTradesSoldGetParams.setFields("tid,status,created,pay_time");

        YouzanTradesSoldGet youzanTradesSoldGet = new YouzanTradesSoldGet();
        youzanTradesSoldGet.setAPIParams(youzanTradesSoldGetParams);
        YouzanTradesSoldGetResult result = client.invoke(youzanTradesSoldGet);
        // client.close();
        TradeDetailV2[] trades = result.getTrades();
        List<Orders> ordersList = new ArrayList<Orders>();
        if (trades != null && trades.length > 0) {
            Map<String, Orders> maps = ordersService.findAllByDateWithGreater(cal);
            Map<String, Account> wxUsersMap = accountService.getWxUsersList();
            for (TradeDetailV2 tmp : trades) {
                // if (tmp.getStatus().indexOf("TRADE_CLOSED") != -1 ||
                // tmp.getStatus().indexOf("TRADE_INVALID") != -1) {
                // log.info("订单[{}]状态不正常，已忽略:{}，{}", tmp.getTid(),
                // tmp.getStatus(), tmp.getStatusStr());
                // continue;
                // }
                Orders orders2 = maps.get(tmp.getTid());
                Orders orders = new Orders();
                FansInfo fansInfo = tmp.getFansInfo();
                orders.setBuyerId(fansInfo.getBuyerId());
                orders.setCreated(tmp.getCreated());
                orders.setFansId(fansInfo.getFansId());
                orders.setFansNickname(fansInfo.getFansNickname());
                orders.setFansType(fansInfo.getFansType());
                orders.setFansWeixinOpenid(fansInfo.getFansWeixinOpenid());
                if (orders2 != null) {
                    if (tmp.getUpdateTime().equals(orders2.getUpdateTime())) {
                        log.info("订单[{}]不需要更新,最新更新时间为[{}]", tmp.getTid(), tmp.getUpdateTime());
                        continue;
                    }
                    orders.setDoctorAccount(orders2.getDoctorAccount());
                    orders.setParentAccount(orders2.getParentAccount());
                    orders.setAddTime(orders2.getAddTime());
                    log.info("更新订单[{}],创建时间[{}],更新时间为[{}],销售[{}],医生[{}]", tmp.getTid(), tmp.getCreated(), tmp.getUpdateTime(),
                            orders.getParentAccount(), orders.getDoctorAccount());
                } else {
                    // 当更新时间更改时，更新相关字段，而用户相关字段不会更新
                    orders.setAddTime(new Date());
                    Account account = wxUsersMap.get(orders.getFansWeixinOpenid());
                    if (account != null) {
                        if (account.getUserType() == 1) {
                            // 销售
                            orders.setParentAccount(account.getAccount());
                        } else {
                            // 医生
                            orders.setParentAccount(account.getParent());
                        }
                        orders.setDoctorAccount(account.getAccount());
                    }
                    log.info("新增订单[{}],创建时间为[{}],销售[{}],医生[{}]", tmp.getTid(), tmp.getCreated(), orders.getParentAccount(),
                            orders.getDoctorAccount());
                }
                // System.out.println(JsonUtils.toJson(tmp));

                orders.setItemId(tmp.getItemId());
                orders.setLat(tmp.getLat());
                orders.setLng(tmp.getLng());
                orders.setReceiverCity(tmp.getReceiverCity());
                orders.setReceiverDistrict(tmp.getReceiverDistrict());
                orders.setReceiverMobile(tmp.getReceiverMobile());
                orders.setReceiverName(tmp.getReceiverName());
                orders.setReceiverState(tmp.getReceiverState());
                orders.setRefundState(tmp.getRefundState());
                orders.setStatus(tmp.getStatus());
                orders.setTid(tmp.getTid());
                orders.setTotalFee(tmp.getTotalFee());
                orders.setPayment(tmp.getPayment());
                orders.setStatusStr(tmp.getStatusStr());
                orders.setPicThumbPath(tmp.getPicThumbPath());
                orders.setTitle(tmp.getTitle());
                orders.setNum(tmp.getNum());
                orders.setUpdateTime(tmp.getUpdateTime());
                orders.setPayTime(tmp.getPayTime());

                ordersList.add(orders);
            }
            ordersRepository.saveAll(ordersList);
        }
    }

}
