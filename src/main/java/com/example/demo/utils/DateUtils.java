package com.example.demo.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {

    static Logger logger = LoggerFactory.getLogger(DateUtils.class);

    public static final String GMT = "GMT+00:00";

    public static final String YYYYMMDD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_HH_mm = "yyyy-MM-dd HH:mm";

    public static final String MMDD = "MM-dd";

    public static final String HHMMSS = "HH:mm:ss";

    public static final String HHMM = "HH:mm";

    public static final String HH = "HH";

    public static final String ALL = "yyyy-MM-dd HH:mm:ss";

    public static final String ALL2 = "yyyy/MM/dd HH:mm:ss";

    public static final String ALL3 = "yyyy-MM-dd-HH:mm:ss.SSS";

    public static final String SUFFIXHHMMSS = " 00:00:00";

    public static final String TIMESTAMP = "yyyyMMddHHmmssSSSS";

    public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";

    public static final String yyyyMMdd = "yyyyMMdd";

    public static final String zcYYYYMMdd = "yyyy年MM月dd日";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_HHDD = "yyyy-MM-dd_HH";

    public static void main(String[] args) throws Exception {
        System.out.println(DateUtils.string2Date("2018-05-20 12:00:00", ALL).getTime());
        System.out.println(DateUtils.string2Date("2018-05-20 19:00:00", ALL).getTime());
        /*
         * System.out.println(DateUtils.string2Date("2018-05-03 00:00:00",
         * ALL).getTime());
         */
        Calendar cal = Calendar.getInstance();
        /*
         * cal.setTime(DateUtils.string2Date("1970-01-01 00:00:00:00", ALL));
         * cal.add(Calendar.SECOND, 1525594785);
         */
        cal.setTimeInMillis(1526815019000L);
        System.out.println(DateUtils.long2Datestr(cal.getTimeInMillis(), ALL));

        Map<String, Date> currentMonth = getCurrentMonth(1, 2018, ALL);
        System.out.println(currentMonth);

    }

    public static String date2String(Date date, String formatType) {
        DateFormat df = new SimpleDateFormat(formatType);
        return df.format(date);
    }

    public static Integer date2Integer(Date date, String formatType) {
        DateFormat df = new SimpleDateFormat(formatType);
        return Integer.parseInt(df.format(date));
    }

    public static Date string2Date(String date, String formatType) {
        DateFormat df = new SimpleDateFormat(formatType);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static String string2String(String date, String formatType, String returnFormatType) {
        if (date == null || date.trim().equals("")) {
            return null;
        }
        DateFormat df = new SimpleDateFormat(formatType);
        DateFormat rdf = new SimpleDateFormat(returnFormatType);
        try {
            return rdf.format(df.parse(date));
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static String long2Datestr(long source, String formatType) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(source);
        return date2String(cal.getTime(), formatType);
    }

    public static Date long2Date(long source) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(source);
        return cal.getTime();
    }

    public static String subDateFormat(String source, Date target) throws ParseException {
        DateFormat df = new SimpleDateFormat(HHMMSS);
        long sp = df.parse(source).getTime();
        long dp = df.parse(df.format(target)).getTime();
        long re = Math.abs(dp - sp);
        df.setTimeZone(TimeZone.getTimeZone(GMT));
        return df.format(re);
    }

    // 获取相差天数
    public static long getDay(String end, String begin) {
        DateFormat df = new SimpleDateFormat(YYYYMMDD);
        long days = 0;
        try {
            Date d1 = df.parse(end);
            Date d2 = df.parse(begin);
            long diff = d1.getTime() - d2.getTime();
            days = diff / (1000 * 60 * 60 * 24);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return days;
    }

    // 获取相差天数
    public static long getDay(Date end, Date begin) {
        long days = 0;
        try {
            long diff = end.getTime() - begin.getTime();
            days = diff / (1000 * 60 * 60 * 24);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return days;
    }

    /**
     * 取得当月天数
     */
    public static int getCurrentMonthLastDay(Date date) {
        Calendar a = Calendar.getInstance();
        a.setTime(date);
        a.set(Calendar.DATE, 1);// 把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);// 日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 得到指定月的天数
     */
    public static int getMonthLastDay(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);// 把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);// 日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    public static boolean isDateBig(Date source, Date target) throws ParseException {
        boolean isBig = true;
        SimpleDateFormat sf = new SimpleDateFormat(ALL);
        long sourceFormat = sf.parse(sf.format(source)).getTime();
        long targetFormat = sf.parse(sf.format(target)).getTime();
        if (sourceFormat - targetFormat < 0) {
            isBig = false;
        }
        return isBig;
    }

    public static boolean compare(Date source, Date target) {
        SimpleDateFormat sf = new SimpleDateFormat(YYYYMMDD);
        String sourceFormat = sf.format(source);
        String targetFormat = sf.format(target);
        return sourceFormat.equals(targetFormat);
    }

    public static long compare(String source, String target) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat(HHMM);
        long sp = sf.parse(source).getTime();
        long dp = sf.parse(target).getTime();
        return sp - dp;
    }

    public static long compare(String source, Date target) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat(HHMMSS);
        long sp = sf.parse(source).getTime();
        long dp = sf.parse(sf.format(target)).getTime();
        return sp - dp;
    }

    public static long compareHHMMSS(String source, String target) {
        SimpleDateFormat sf = new SimpleDateFormat(HHMMSS);
        long sp = 0;
        long dp = 0;
        try {
            sp = sf.parse(source).getTime();
            dp = sf.parse(target).getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            logger.error(e.getMessage(), e);
        }
        return sp - dp;
    }

    public static long compareInAll(String source, String target) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat(ALL);
        long sp = sf.parse(source).getTime();
        long dp = sf.parse(target).getTime();
        return sp - dp;
    }

    // 获得now的当月起止日期yyyy-MM-dd HH:mm:dd
    // 由于sql使用 cur>=start and cur < end,故获取的end为下个月第一天
    public static Map<String, Date> getCurrentMonth(Date now, String formatType) throws ParseException {
        Map<String, Date> re = new HashMap<String, Date>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        re.put("stime", string2Date(date2String(cal.getTime(), formatType), formatType));
        cal.add(Calendar.MONTH, 1);
        re.put("etime", string2Date(date2String(cal.getTime(), formatType), formatType));
        return re;
    }

    // 获得now的当月起止日期yyyy-MM-dd HH:mm:dd
    // 由于sql使用 cur>=start and cur < end,故获取的end为下个月第一天
    public static Map<String, Date> getCurrentMonth(Integer month, Integer year, String formatType) {
        Map<String, Date> re = new HashMap<String, Date>();
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        if (month != null) {
            cal.set(Calendar.MONTH, month - 1);
        }

        if (year != null) {
            cal.set(Calendar.YEAR, year);
        }
        re.put("stime", string2Date(date2String(cal.getTime(), formatType), formatType));
        cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.SECOND, -1);
        re.put("etime", string2Date(date2String(cal.getTime(), formatType), formatType));
        return re;
    }

    // 获得now的本周起止日期yyyy-MM-dd HH:mm:dd，周一到周日
    // 由于sql使用 cur>=start and cur < end,故获取的end为下周一
    public static Map<String, Date> getCurrentWeek(Date now, String formatType) throws ParseException {
        Map<String, Date> re = new HashMap<String, Date>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        int day = cal.get(Calendar.DAY_OF_WEEK) - 1;// 今天是星期的第几天
        cal.add(Calendar.DATE, 1 - day);
        re.put("stime", string2Date(date2String(cal.getTime(), formatType), formatType));
        cal.add(Calendar.DATE, 7);
        re.put("etime", string2Date(date2String(cal.getTime(), formatType), formatType));
        return re;
    }

    // 获取now时间上加years年后的format格式日期
    public static Date getIncreYearDate(Date now, int years, String format) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.YEAR, years);
        Date date = string2Date(date2String(cal.getTime(), format), format);
        return date;
    }

    // 获取now时间上加days（可为负数）天后的format格式日期
    public static Date getIncreDate(Date now, int days, String format) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, days);
        Date date = string2Date(date2String(cal.getTime(), format), format);
        return date;
    }

    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    public static Date getDateFromUK(String date) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
        return sdf1.parse(date);
    }

    public static final String[] CHINESEWEEKARRYS = new String[] { "日", "一", "二", "三", "四", "五", "六" };

    public static String getWeekName(long million) {
        Calendar ca = Calendar.getInstance();
        ca.setTimeInMillis(million);
        return CHINESEWEEKARRYS[ca.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static final Integer[] INT_CHINESEWEEKARRYS = new Integer[] { 7, 1, 2, 3, 4, 5, 6 };

    public static Integer getIntWeekName(long million) {
        Calendar ca = Calendar.getInstance();
        ca.setTimeInMillis(million);
        return INT_CHINESEWEEKARRYS[ca.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static String cal2SqlString(Calendar cal) {
        return " '" + date2String(cal.getTime(), ALL) + "' ";
    }

    /*
     * 通过日期获得周几
     */
    public static String[] WEEKNAME = { "一", "二", "三", "四", "五", "六", "日" };

    public static String getWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        day--;
        if (day == 0) {
            day = 7;
        }
        return WEEKNAME[--day];
    }

    // 获取上i个整点
    public static String getLast(int i) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - i);
        return sdf.format(calendar.getTime());
    }

    // 获取上个时间的半点 如 当前是14:02 得到13:30
    public static String getLast() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:30:00");
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
        return sdf.format(calendar.getTime());
    }
}
