package com.example.demo.utils;

import java.util.Calendar;
import java.util.Date;

import com.example.demo.bean.EventCallBack;
import com.example.demo.entity.CustomerList;

public class Event2Customer {

    private Event2Customer() {
        throw new IllegalStateException("Utility class");
    }

    public static CustomerList convert(EventCallBack event) {
        CustomerList customer = new CustomerList();
        if (event.getEvent().startsWith("subscribe")) {
            customer.setAccount(event.getEventKey().split("_")[1]);
        }
        customer.setAddTime(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.valueOf(event.getCreateTime()));
        customer.setCreateTime(cal.getTime());
        customer.setEvent(event.getEvent());
        customer.setFromUserName(event.getFromUserName());
        customer.setMsgType(event.getMsgType());
        customer.setTicket(event.getTicket());
        customer.setToUserName(event.getToUserName());
        return customer;
    }
}
