package com.example.demo.utils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.fasterxml.jackson.databind.ObjectMapper;

/*
 *	作用：提供http连接池机制，发送http请求到第三方服务器
 *     调用方式：http get
 *     针对一个服务器，建立一个http连接池
 */
public class HttpClientPool {
    private static Logger logger = LoggerFactory.getLogger(HttpClientPool.class);

    private static CloseableHttpClient httpclient = null;

    static {
        PoolingHttpClientConnectionManager phcm = new PoolingHttpClientConnectionManager(30, TimeUnit.SECONDS);
        phcm.setMaxTotal(100);
        phcm.setDefaultMaxPerRoute(100);
        phcm.closeExpiredConnections();
        phcm.closeIdleConnections(300, TimeUnit.SECONDS);
        @SuppressWarnings("deprecation")
        RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY).setSocketTimeout(5000)
                .setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpclient = HttpClients.custom().setConnectionManager(phcm).setDefaultRequestConfig(globalConfig).build();

    }

    /**
     * post
     * 
     * @param jsonString
     *            json参数
     * @param params
     *            url后附件的参数
     * @param httpAddress
     *            请求地址
     * @return json格式字符串
     * 
     */
    public static String invokeWithUrlParamsAndJson(String jsonString, Map<String, String> strings, String httpAddress) {
        String responseBody = null;
        try {
            StringBuilder calHttp = new StringBuilder(httpAddress);
            if (strings != null && !strings.isEmpty()) {
                int i = 0;
                for (String key : strings.keySet()) {
                    String value = strings.get(key);
                    if (i == 0) {
                        calHttp.append("?" + key + "=" + value);
                    } else {
                        calHttp.append("&" + key + "=" + value);
                    }
                    i++;
                }
            }
            HttpPost httpost = new HttpPost(calHttp.toString());
            StringEntity s = new StringEntity(jsonString);
            s.setContentEncoding("UTF-8");
            s.setContentType("application/json");
            httpost.setEntity(s);

            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                        // return entity != null ?
                        // convertStreamToString(entity.getContent()) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            responseBody = httpclient.execute(httpost, responseHandler);
            logger.info(responseBody);
        } catch (Exception e) {
            logger.error("http调用异常", e);
        }
        return responseBody;
    }

    public static String invokeGet(String address) {
        String responseBody = null;
        try {
            HttpGet httpget = new HttpGet(address);
            logger.debug("Executing request " + httpget.getRequestLine());
            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                        // return entity != null ?
                        // convertStreamToString(entity.getContent()) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            responseBody = httpclient.execute(httpget, responseHandler);
            logger.info(responseBody);
        } catch (IOException e) {
            logger.error(address, e);
        } finally {
            // httpclient.close();
        }
        return responseBody;
    }

    /**
     * @param address
     *            请求地址
     * @param Params
     *            请求的字符串 x-www-form-urlencoded格式
     * 
     * @return 返回字符串数据
     */
    public static String invokePostWithForm(String address, String params) {
        String responseBody = null;
        try {
            HttpPost httpget = new HttpPost(address);
            httpget.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            StringEntity entity = new StringEntity(params, "UTF-8");
            entity.setContentEncoding("UTF-8");
            httpget.setEntity(entity);
            logger.debug("Executing request " + httpget.getRequestLine());
            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                        // return entity != null ?
                        // convertStreamToString(entity.getContent()) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            responseBody = httpclient.execute(httpget, responseHandler);
            logger.info(responseBody);
        } catch (IOException e) {
            logger.error(address, e);
        } finally {
            // httpclient.close();
        }
        return responseBody;
    }

    /**
     * @param address
     *            请求地址
     * @param Params
     *            请求的字符串 x-www-form-urlencoded格式
     * 
     * @return 返回字符串数据
     */
    public static String invokePost(String address, File file, String fileName) {
        String responseBody = null;
        try {
            HttpPost httpget = new HttpPost(address);
            // httpget.addHeader("Content-Type", "multipart/form-data");
            // StringEntity entity = new StringEntity(params, "UTF-8");
            // entity.setContentEncoding("UTF-8");
            // httpget.setEntity(entity);
            HttpEntity me = MultipartEntityBuilder.create().addBinaryBody(fileName, file).build();
            httpget.setEntity(me);
            logger.debug("Executing request " + httpget.getRequestLine());
            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                        // return entity != null ?
                        // convertStreamToString(entity.getContent()) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            responseBody = httpclient.execute(httpget, responseHandler);
            logger.info(responseBody);
        } catch (IOException e) {
            logger.error(address, e);
        } finally {
            // httpclient.close();
        }
        return responseBody;
    }

    // /*
    // * 告警信息发送至微信告警组
    // */
    // public static void invokeWxchat4WarnMsg(String msg) {
    // // 微信开关
    // String valueByRealTime =
    // FileLoadUtil.getValueByRealTime("warning.wxchat.switch");
    // Integer value = Integer.valueOf(valueByRealTime.trim());
    // if (value == 1) {
    // return;
    // }
    // String getLocation = null;
    // String addr =
    // FileLoadUtil.getInstance().getKeyValueByKeyName("default.send.wxchat.url");
    // // + "msg=" + msg;
    // Map<String, String> params = new HashMap<String, String>();
    // // if (tagId != null && !tagId.trim().equals("")) {
    // // params.put("tagId", tagId);
    // // }
    // params.put("msg", msg.replaceAll(",", ",\r\n"));
    // try {
    // getLocation = invokePost(addr, params);
    // if (getLocation == null || getLocation.trim().equals("")) {
    // logger.error("微信服务器返回数据为空");
    // }
    // logger.info(">>>{}", getLocation);
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // logger.error(e.getMessage(), e);
    // }
    // }
    //
    // /*
    // * 告警信息发送至微信告警组
    // *
    // * @param tagId 默认为0
    // *
    // * @param agentId 默认为0
    // */
    // public static void invokeWxchat4WarnMsg(String msg, String tagId, Integer
    // agentId) {
    // // 微信开关
    // String valueByRealTime =
    // FileLoadUtil.getValueByRealTime("warning.wxchat.switch");
    // Integer value = Integer.valueOf(valueByRealTime.trim());
    // if (value == 1) {
    // return;
    // }
    // String getLocation = null;
    // String addr =
    // FileLoadUtil.getInstance().getKeyValueByKeyName("default.send.wxchat.url");
    // // + "msg=" + msg;
    // Map<String, String> params = new HashMap<String, String>();
    // if (tagId != null && !tagId.trim().equals("")) {
    // params.put("tagId", tagId);
    // }
    // if (agentId != null) {
    // params.put("agentId", agentId + "");
    // }
    // params.put("msg", msg.replaceAll(",", ",\r\n"));
    // try {
    // getLocation = invokePost(addr, params);
    // if (getLocation == null || getLocation.trim().equals("")) {
    // logger.error("微信服务器返回数据为空");
    // }
    // logger.info(">>>{}", getLocation);
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // logger.error(e.getMessage(), e);
    // }
    // }
    //
    // public static void main(String[] args) {
    // String title = "布控人员报警:" + "\r\ndklfjajdf" + "[" + "imsi" + ":" + "imei"
    // +
    // "]出现," + "设备号[" + 12 + "],信号强度[" + 80 + "],归属地[" + "北京 北京"
    // + "],时间[" + "2015-05-01 00:00:00" + "]" + "\r\n@lilian微信测试";
    // invokeWxchat4WarnMsg(title, "1", null);
    // }
}
