
package com.example.demo.utils;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年8月19日 下午9:43:42
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Slf4j
public class JsonUtils {

    private static ObjectMapper om = new ObjectMapper();

    public static <T> T deserial(String jsonStr, Class<T> t) {
        T readValue = null;
        try {
            readValue = om.readValue(jsonStr, t);
        } catch (IOException e) {
            log.error("json反序列化出错:" + jsonStr, e);
        }
        return readValue;
    }

    public static String serial(Object obj) {
        String jsonStr = null;
        try {
            StringWriter sw = new StringWriter();
            om.writeValue(sw, obj);
            jsonStr = sw.toString();
        } catch (IOException e) {
            log.error("json序列化出错:" + obj, e);
        }
        return jsonStr;
    }
}
