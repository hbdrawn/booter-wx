package com.example.demo.utils;

import java.util.HashMap;
import java.util.Map;

import com.example.demo.bean.QrcodeRespond;
import com.example.demo.entity.Account;

public class QrcodeCreateUtils {

    public static final String CREATE_POST_JSON_PREFIX = "{\"action_name\": \"QR_LIMIT_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"";

    public static final String CREATE_POST_JSON_END = "\"}}}";

    public static final String CREATE_POST_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create";

    public static final String SHOW_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";

    /*
     * 创建用户的二维码信息,并与用户信息做关联
     */
    public static void create(Account user) {
        StringBuilder sb = new StringBuilder(CREATE_POST_JSON_PREFIX);
        sb.append(user.getAccount());
        sb.append(CREATE_POST_JSON_END);
        Map<String, String> params = new HashMap<String, String>();
        params.put("access_token", SysConstant.token);
        String paramsAndJson = HttpClientPool.invokeWithUrlParamsAndJson(sb.toString(), params, CREATE_POST_URL);
        if (paramsAndJson != null) {
            QrcodeRespond res = SerialUtils.deserial(paramsAndJson, QrcodeRespond.class);
            // 将用户信息回填
            if (res.getErrcode() == null) {
                user.setTicket(res.getTicket());
                user.setUrl(res.getUrl());
                user.setExpire(res.getExpire_seconds());
            }
        }
    }
}
