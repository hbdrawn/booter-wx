package com.example.demo.utils;

import java.io.IOException;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SerialUtils {
    private static Logger logger = LoggerFactory.getLogger(SerialUtils.class);

    public static ObjectMapper om = new ObjectMapper();

    public static <T> T deserial(String jsonStr, Class<T> t) {
        T readValue = null;
        try {
            readValue = om.readValue(jsonStr, t);
        } catch (IOException e) {
            logger.error("json反序列化出错:" + jsonStr, e);
        }
        return readValue;
    }

    public static String serial(Object obj) {
        StringWriter sw = new StringWriter();
        try {
            om.writeValue(sw, obj);
        } catch (IOException e) {
            logger.error("json序列化出错:" + obj.toString(), e);
        }
        return sw.toString();
    }
}
