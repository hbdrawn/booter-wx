package com.example.demo.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.env.Environment;

import com.example.demo.bean.AccessTocken4Url;
import com.example.demo.bean.WxUserInfo;
import com.example.demo.bean.WxUserList;
import com.example.framework.config.SpringUtil;

public class WxUsersServiceUtils {

    public static String userListurl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";

    public static String userUrl = "https://api.weixin.qq.com/cgi-bin/user/info?lang=zh_CN&access_token=";

    public static String code2JsTokenUrl = "https://api.weixin.qq.com/sns/jscode2session?grant_type=authorization_code&js_code=";

    public static List<WxUserInfo> getUserList() {
        List<WxUserInfo> all = new ArrayList<WxUserInfo>();
        String invokeGet = HttpClientPool.invokeGet(userListurl + SysConstant.token);
        if (invokeGet != null && invokeGet.indexOf("errcode") == -1) {
            WxUserList deserial = SerialUtils.deserial(invokeGet, WxUserList.class);
            // TODO 1w+时需要循环遍历
            for (String tmp : deserial.getIds()) {
                String invokeGet2 = HttpClientPool.invokeGet(userUrl + SysConstant.token + "&openid=" + tmp);
                WxUserInfo deserial2 = SerialUtils.deserial(invokeGet2, WxUserInfo.class);
                // WxUserInfo newInfo = new WxUserInfo();
                // newInfo.setNickname(deserial2.getNickname());
                // newInfo.setOpenid(deserial2.getOpenid());
                // newInfo.setQr_scene_str(deserial2.getQr_scene_str());
                // newInfo.setSex(deserial2.getSex());
                // newInfo.setHeadimgurl(deserial2.getHeadimgurl());
                // newInfo.setUnionid(deserial2.getUnionid());
                // newInfo.setCity(deserial2.getCity());
                all.add(deserial2);
            }
        }
        return all;
    }

    public static WxUserInfo getUserInfo(String openId) {
        WxUserInfo deserial = null;
        String invokeGet = HttpClientPool.invokeGet(userUrl + SysConstant.token + "&openid=" + openId);
        if (invokeGet != null && invokeGet.indexOf("errcode") == -1) {
            deserial = SerialUtils.deserial(invokeGet, WxUserInfo.class);
        }
        return deserial;
    }

    public static AccessTocken4Url getOpenId(String code) {
        Environment environment = SpringUtil.getBean(Environment.class);
        String invokeGet = HttpClientPool.invokeGet(code2JsTokenUrl + code + "&appid=" + environment.getProperty("weixin.xc-app-id") + "&secret="
                + environment.getProperty("weixin.xc-secret"));
        if (invokeGet != null && invokeGet.indexOf("errcode") == -1) {
            AccessTocken4Url accessTocken = SerialUtils.deserial(invokeGet, AccessTocken4Url.class);
            return accessTocken;
        }
        return null;
    }

}
