package com.example.demo.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XMLUtils {

    /**
     * 将传入xml文本转换成Java对象
     * 
     * @Title: toBean
     * @Description: TODO
     * @param xmlStr
     * @param cls
     *            xml对应的class类
     * @return T xml对应的class类的实例对象
     * 
     *         调用的方法实例：PersonBean person=XmlUtil.toBean(xmlStr,
     *         PersonBean.class);
     */
    public static <T> T toBean(String xmlStr, Class<T> cls) {
        // 注意：不是new Xstream(); 否则报错：java.lang.NoClassDefFoundError:
        // org/xmlpull/v1/XmlPullParserFactory
        XStream xstream = new XStream(new DomDriver());
        xstream.processAnnotations(cls);
        @SuppressWarnings("unchecked")
        T obj = (T) xstream.fromXML(xmlStr);
        return obj;
    }

}
