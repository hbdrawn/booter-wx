/**
 * 
 */
package com.example.framework.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import com.example.demo.utils.SessionConstant;
import com.example.framework.annotation.MethodPermision;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangyj
 *
 */
@Slf4j
public class LoginIntercepter implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {// 对资源类加载特殊处理
        if (!(handler instanceof ResourceHttpRequestHandler)) {
            // 处理Permission Annotation，实现方法级权限控制
            HandlerMethod method = (HandlerMethod) handler;
            log.debug("客户端[{}]发送请求[{}]", new Object[] { request.getHeader("user-agent"), method.getMethod().getName() });

            MethodPermision permission = method.getMethodAnnotation(MethodPermision.class);
            // 如果为空在表示该方法不需要进行权限验证
            if (permission != null) {
                return true;
            }
        }
        HttpSession session = request.getSession(false);
        if (session == null
                || (session.getAttribute(SessionConstant.SESSION_ID) == null && session.getAttribute(SessionConstant.WEIXIN_LOGIN) == null)) {
            response.sendRedirect(request.getContextPath() + "/admin/index");
            // ajax调用时，此语句无效
            // response.sendRedirect(request.getContextPath() +
            // "/views/login.jsp");
            // response.flushBuffer();
            return false;
            // SysUser user = new SysUser();
            // user.setId(1);
            // user.setLname("system");
            // user.setRoletype(0);
            // user.setStatus(0);
            // session.setAttribute("loginUser", user);
        }

        // if (HttpRequestDeviceUtils.isMobileDevice(request)) {
        // request.getRequestDispatcher("/m/" +
        // request.getRequestURI()).forward(request, response);
        // }
        return true;
    }

    /*
     * @Override public void postHandle(HttpServletRequest request,
     * HttpServletResponse response, Object handler, @Nullable ModelAndView
     * modelAndView) throws Exception { }
     * 
     * @Override public void afterCompletion(HttpServletRequest request,
     * HttpServletResponse response, Object handler, @Nullable Exception ex)
     * throws Exception { }
     */
}
