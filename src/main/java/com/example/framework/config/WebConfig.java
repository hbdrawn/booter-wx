package com.example.framework.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
@EnableWebMvc
@ComponentScan
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // registry.addResourceHandler("/**").addResourceLocations("/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截规则：除了login，其他都拦截判断
        registry.addInterceptor(new LoginIntercepter()).addPathPatterns("/**").excludePathPatterns("/**/*.css").excludePathPatterns("/**/*.txt")
                .excludePathPatterns("/**/*.js").excludePathPatterns("/test**/**").excludePathPatterns("/**/*.jpg")
                .excludePathPatterns("/admin/index").excludePathPatterns("/admin/login").excludePathPatterns("/event/**")
                .excludePathPatterns("/youzan/callback/**").excludePathPatterns("/websocket");
        super.addInterceptors(registry);
    }

}
