
package com.example.realtime;

import lombok.Data;

/**
 * .描述:
 * 
 * @author zhangyj 
 * @date 2018年9月10日 下午1:06:12
 * @version 
 * 
 *            CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Data
public class MsgBody {

    private String type;
    private String content;
}
