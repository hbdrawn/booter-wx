
package com.example.realtime;

import java.util.Date;

import com.example.demo.utils.DateUtils;
import com.example.realtime.utils.ConversationsType;

import lombok.Data;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年8月19日 下午8:47:19
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/

@Data
public class MsgWrapper implements Cloneable {

    private ConversationsType type;

    /**
     * content_type = 0 text
     * 
     * content_type = 1 voice
     * 
     * content_type = 2 自定义
     */
    private String content;
    /**
     * .总未读数据
     */
    private int unread;
    private Date timestamp = new Date();

    private String friendId;// 消息发送者
    private String friendHeadUrl;
    private String friendName;
    private String latestMsg;// item.content最新一条消息
    private String timeStr = DateUtils.date2String(timestamp, DateUtils.HHMM);
    private String msgUserId;// 消息拥有者
    private String msgUserName;
    private String msgUserHeadUrl;

    /**
     * .暂时未用到
     */
    private String conversationId;

    private String code;
    private String iv;

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        setTimeStr(DateUtils.date2String(timestamp, DateUtils.HHMM));
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
