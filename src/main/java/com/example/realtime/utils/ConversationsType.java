
package com.example.realtime.utils;

/**
 * .描述:
 * 
 * @author zhangyj
 * @date 2018年8月19日 下午9:37:54
 * @version
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/

public enum ConversationsType {

    get_conversations, get_history, get_friends, get_login, text, voice, image, custom;

}
