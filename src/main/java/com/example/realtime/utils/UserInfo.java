package com.example.realtime.utils;

import lombok.Data;

/**
 * .描述:websocket登陆用户信息
 * 
 * @author zhangyj
 * @date 2018年9月8日 下午2:38:31
 * @version 1.0.0
 * 
 *          CopyRight (c) 2018 , hbdrawn@vip.qq.com All Rights Reserved
 *
 **/
@Data
public class UserInfo {
    private String userId;
    private String nickName;
    private String myHeadUrl;

    /**
     * 0=销售 1=医生 2=关注用户
     */
    private Integer userType;
}
