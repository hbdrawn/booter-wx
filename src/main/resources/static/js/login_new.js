//$(function() {
//
//});

function login() {
	var jsonuserinfo = $('#login_form').serializeObject();
	var jsonStr = JSON.stringify(jsonuserinfo);
	$.ajax({
		// 几个参数需要注意一下
		type : "POST",// 方法类型
		dataType : "json",// 预期服务器返回的数据类型
		url : "/admin/login",// url
		contentType : "application/json;charset=utf-8",
		data : jsonStr,
		// data : "account=" + user,
		success : function(result) {
			console.log(result);// 打印服务端返回的数据(调试用)
			// if (result.resultCode == 200) {
			$('#msg').html(result.msg);
			$('#warning').modal('show');
			if (result.code == 1) {
				window.location.href = '/admin/suc';
			}
			// }
		},
		error : function() {
			$('#msg').html('服务器操作异常');
			$('#warning').modal('show');
		}
	});
}