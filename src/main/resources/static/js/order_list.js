$(function() {
	var oTable = new TableInit();
	oTable.Init();

	// 2.初始化Button的点击事件
	var oButtonInit = new ButtonInit();
	oButtonInit.Init();

});

var TableInit = function() {
	var oTableInit = new Object();
	// 初始化Table
	oTableInit.Init = function() {
		$('#tb_departments')
				.bootstrapTable(
						{
							url : '/order/page', // 请求后台的URL（*）
							method : 'get', // 请求方式（*）
							toolbar : '#toolbar', // 工具按钮用哪个容器
							striped : true, // 是否显示行间隔色
							cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
							pagination : true, // 是否显示分页（*）
							sortable : false, // 是否启用排序
							sortOrder : "asc", // 排序方式
							queryParams : oTableInit.queryParams,// 传递参数（*）
							sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
							pageNumber : 1, // 初始化加载第一页，默认第一页
							pageSize : 10, // 每页的记录行数（*）
							pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
							search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
							strictSearch : true,
							showColumns : true, // 是否显示所有的列
							showRefresh : true, // 是否显示刷新按钮
							minimumCountColumns : 2, // 最少允许的列数
							clickToSelect : true, // 是否启用点击选中行
							// height : 500, //
							// 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
							uniqueId : "tid", // 每一行的唯一标识，一般为主键列
							showToggle : true, // 是否显示详细视图和列表视图的切换按钮
							cardView : false, // 是否显示详细视图
							detailView : false, // 是否显示父子表
							columns : [
									{
										field : 'picThumbPath',
										align : 'center',
										title : '商品',
										formatter : function(value,row,index){
											return '<img style="max-width:30%;max-height:20%;" src=' + value + '/>';
										}
									},
									{
										field : 'fansNickname',
										align : 'center',
										title : '购买者'
									},
									{
										field : 'totalFee',
										align : 'center',
										title : '总金额'
									},
									{
										field : 'payment',
										align : 'center',
										title : '已支付'
									},
									{
										field : 'statusStr',
										align : 'center',
										title : '订单状态'
									},
									{
										field : 'created',
										align : 'center',
										title : '下单时间'
									},
									{
										field : 'receiverCity',
										align : 'center',
										title : '地点'
									}]
						});
	};

	// 得到查询的参数
	oTableInit.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
// userType : 2,
			pageSize : params.limit, // 页面大小
			pageNumber : params.offset
		// 页码
		// departmentname : $("#txt_search_departmentname").val(),
		// statu : $("#txt_search_statu").val()
		};
		return temp;
	};
	return oTableInit;
};

var ButtonInit = function() {
	var oInit = new Object();
	var postdata = {};

	oInit.Init = function() {
		// 初始化页面上面的按钮事件
	};

	return oInit;
};

function getOrderDisplayHtml(value,row,index){
	return '<div class="container-fluid">' + 
	'<div class="row-fluid"><div class="span12"><div class="row-fluid">' + 
	'<div class="span4"><img src="'+ row.picThumbPath +'"/></div>' + 
	'<div class="span8">' + 
	'<div class="row-fluid"><div class="span12">' + row.title + '</div></div>' +
	'<div class="row-fluid"><div class="span12"><div class="row-fluid">' + 
	'<div class="span4">个数:' + row.num + '</div>'+
	'<div class="span4">总费用:' + row.totalFee + '</div>' + 
	'<div class="span4">已支付费用:'+ row.payment + '</div>' + 
	'</div></div></div>' + 
	'<div class="row-fluid"><div class="span12"><div class="row-fluid"><div class="span4">购买者:' + row.receiverName + 
	'</div><div class="span8">时间:' + row.addTime + '</div></div></div></div>' + 
	'<div class="row-fluid"><div class="span12"><div class="row-fluid"><div class="span4">订单状态:' + row.statusStr + 
	'</div><div class="span8">地点:' + row.receiverCity + 
	'</div></div></div></div></div></div></div></div></div>';
}