/* (function(){
 var num=1/document.devicePixelRatio;
 var meta=document.createElement('meta');
 meta.name='viewport';
 meta.cont='width=device-width, user-scalable=no, initial-scale='+num+', maximum-scale='+num+', minimum-scale='+num;
 document.body.appendChild(meta);
 var width=document.documentElement.clientWidth/20;
 document.getElementsByTagName('html')[0].style.fontSize=width+'px';
 })();*/
(function () {
    var html = document.documentElement;

    function onWindowResize() {

        html.style.fontSize = html.getBoundingClientRect().width / 20 + 'px';
       
    }

    window.addEventListener('resize', onWindowResize);
    onWindowResize();
})();
