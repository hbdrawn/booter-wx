package com.example.demo.service;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.example.demo.DemoApplicationTests;
import com.example.demo.bean.WxUserInfo;
import com.example.demo.entity.Account;
import com.example.demo.repository.AcountRepository;
import com.example.demo.utils.WxUsersServiceUtils;

public class AccountTest extends DemoApplicationTests {

    @Autowired
    AccountService accountService;

    @Autowired
    private AcountRepository accountRepository;

    @Test
    public void testAdd() {
        Account account = new Account();
        account.setAccount("test1");
        account.setNickname("hbdrawn");
        account.setPhone("18701585847");
        account.setState(1);
        account.setUserType(1);
        Map<String, Object> add = accountService.add(account);
        assertEquals(1, add.get("code"));

        accountService.deleteById(account.getAccount());

        Optional<Account> findById = accountService.findById(account.getAccount());
        assertEquals(false, findById.isPresent());
    }

    @Test
    public void testAddAndGetTicket() throws InterruptedException {
        Account account = new Account();
        account.setAccount("test1");
        account.setNickname("hbdrawn");
        account.setPhone("18701585847");
        account.setState(1);
        account.setUserType(2);
        /* Thread.sleep(3000); */
        Map<String, Object> add = accountService.add(account);
        assertEquals(2, add.get("code"));

        /*
         * accountService.deleteById(account.getAccount());
         * 
         * Optional<Account> findById =
         * accountService.findById(account.getAccount()); assertEquals(false,
         * findById.isPresent());
         */

    }

    @Test
    public void testFindAll() {
        List<Account> list = accountService.findAll();
        System.out.println(list.size());
        for (Account tmp : list) {
            if(tmp.getOpenId() == null) {
                continue;
            }
            WxUserInfo userInfo = WxUsersServiceUtils.getUserInfo(tmp.getOpenId());
            if(userInfo == null) {
                System.out.println(tmp.getAccount());
                continue;
            }
            tmp.setUnionId(userInfo.getUnionid());
            accountRepository.save(tmp);
        }
    }

    @Test
    public void testFindByParent() {
        Pageable page = PageRequest.of(0, 10, Sort.by("addTime"));
        Page<Account> list = accountService.findDoctorPageByAccount("A0001", page);
        System.out.println(list);
    }

    @Test
    public void testFindByOpenId() {
        Optional<Account> list = accountService.findByOpenId("oKhLb0YZtFJM6dxP8_gqdt1UueO4");
        System.out.println(list);
    }
}
