package com.example.demo.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.DemoApplicationTests;
import com.example.demo.entity.CustomerList;
import com.example.demo.repository.CustomerRepository;

public class CustomerListTest extends DemoApplicationTests {

    @Autowired
    CustomerRepository customerRepository;

    @Test
    public void test3Condition() {
        List<CustomerList> userNameAndStatus = customerRepository.findByFromUserNameAndStatus("oKhLb0YZtFJM6dxP8_gqdt1UueO4", 1);
        for (CustomerList tmp : userNameAndStatus) {
            tmp.setStatus(2);
            customerRepository.save(tmp);
        }
        System.out.println(1);
    }

}
