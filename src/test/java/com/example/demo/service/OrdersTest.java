package com.example.demo.service;

import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.DemoApplicationTests;
import com.example.demo.bean.OrdersDisplay;
import com.example.demo.bean.OrdersDisplay4Mlogin;
import com.example.demo.entity.Orders;
import com.example.demo.utils.DateUtils;

public class OrdersTest extends DemoApplicationTests {

    @Autowired
    OrdersService ordersService;

    @Autowired
    Orders4MobileLoginService parentService;

    @Test
    public void testTongji() {
        Date start = DateUtils.string2Date("2018-06-06 00:00:00", DateUtils.ALL);
        Date end = DateUtils.string2Date("2018-06-24 00:00:00", DateUtils.ALL);
        Orders order = new Orders();
        order.setParentAccount("S001001");
        OrdersDisplay infoByDate = ordersService.getInfoByDate(order, start, end);
        System.out.println(infoByDate);
    }

    @Test
    public void testFindAllByDateWithGreater() {
        Date start = DateUtils.string2Date("2018-06-23 19:34:36", DateUtils.ALL);
        Map<String, Orders> infoByDate = ordersService.findAllByDateWithGreater(start);
        System.out.println(infoByDate);
    }

    @Test
    public void testgetOrdersDisplay4Parent() {
        OrdersDisplay4Mlogin display = new OrdersDisplay4Mlogin();
        display.setAccount("S001001");
        OrdersDisplay4Mlogin ordersDisplay4Parent = parentService.getOrdersDisplay4Parent(display);
        System.out.println(ordersDisplay4Parent);
    }

    @Test
    public void testgetOrdersDisplay4Doctor() {
        OrdersDisplay4Mlogin display = new OrdersDisplay4Mlogin();
        display.setAccount("D001001");
        OrdersDisplay4Mlogin ordersDisplay4Parent = parentService.getOrdersDisplay4Doctor(display);
        System.out.println(ordersDisplay4Parent);
    }
}
