package com.example.demo.service;

import java.util.Calendar;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.DemoApplicationTests;
import com.example.demo.utils.DateUtils;

public class YouZanGetOrderServiceTest extends DemoApplicationTests {

    @Autowired
    private YouZanGetOrderService youZanGetOrderService;

    @Test
    public void testOrders() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtils.string2Date("2018-05-01 00:00:00", DateUtils.ALL));
        youZanGetOrderService.getOrderList(cal.getTime());
    }
}
