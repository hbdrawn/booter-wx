package com.example.demo.utils;

import java.net.URLEncoder;

import org.junit.Test;

import com.example.demo.bean.EventCallBack;

public class UtilsTest {

    @Test
    public void testUtils() {
        System.out.println(Integer.MAX_VALUE);
    }

    @Test
    public void testXMLUtils() {
        String demo = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[subscribe]]></Event><EventKey><![CDATA[qrscene_123123]]></EventKey><Ticket><![CDATA[TICKET]]></Ticket></xml>";
        EventCallBack bean = XMLUtils.toBean(demo, EventCallBack.class);
        System.out.println(bean);
    }

    @Test
    public void testEncodingUrl() {
        String url = "http://www.idebug.top/event/login?fromType=2";
        System.out.println(URLEncoder.encode(url));
    }

    @Test
    public void testStringSub() {
        String title = "【的积分姐姐】";
        int index = title.indexOf("【");
        int last = title.indexOf("】");
        if (index != -1 && last != -1) {
            title = title.substring(index + 1, last);
        }
        System.out.println(title);
    }
}
